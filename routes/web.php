<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/',array('uses' => 'HomeController@home'));
Route::get('/udyam-registration',array('uses' => 'HomeController@index'));
Route::get('/msme-to-udyam-registration',array('uses' => 'HomeController@msmeToUdyamRegistration'));
Route::get('/payment/{id}',array('uses' => 'HomeController@payment'))->name('payment');
Route::post('/payment-success',array('uses' => 'HomeController@paymentSucess'));
Route::get('/payment-success/{id}',array('uses' => 'HomeController@getPaymentSucess'));
Route::post('/udyam-form-submit',array('uses' => 'HomeController@udyamRegistrationSubmit'));
Route::post('/msme-udyam-form-submit',array('uses' => 'HomeController@msmeRegistrationSubmit'));
Route::get('/track-application',array('uses' => 'HomeController@trackApplication'))->name('track-application');
Route::post('/track-application-submit',array('uses' => 'HomeController@trackApplicationSubmit'));
Route::get('/udyam-registration-faqs',array('uses' => 'HomeController@faqs'));
Route::get('/udyam-registration-procedure',array('uses' => 'HomeController@procedure'));
Route::get('/udyam-registration-sample-certificate',array('uses' => 'HomeController@certificate'));
Route::get('/udyam-registration-benefits',array('uses' => 'HomeController@benefits'));
Route::get('/udyam-registration-terms-and-condition',array('uses' => 'HomeController@termsAndCondition'));
Route::get('/udyam-registration-privacy-policy',array('uses' => 'HomeController@privacyPolicy'));
Route::get('/udyam-registration-refund-policy',array('uses' => 'HomeController@refundPolicy'));
Route::get('/contact-us',array('uses' => 'HomeController@contactUs'))->name('contact-us');;
Route::post('/contact-us-submit',array('uses' => 'HomeController@contactUsSubmit'));
Route::get('/about-us',array('uses' => 'HomeController@aboutUs'));
Route::get('/mail',array('uses' => 'HomeController@mail'));
Route::get('/home',array('uses' => 'HomeController@home'));
Route::post('/validate-captcha',array('uses' => 'HomeController@validateCaptcha'));
Route::get('/expert-advice-phone',array('uses' => 'ExpertAdviceController@expertAdvicePhone'));
Route::POST('/expert-advice-register',array('uses' => 'ExpertAdviceController@registerCustomerForExpertAdvice'));
Route::POST('/udyam-msme-register-ajax',array('uses' => 'HomeController@registerMSMEAjax'));
Route::post('/msme-payment-success-indirect',array('uses' => 'HomeController@paymentMSMESucessIndirect'));
Route::get('/mailea',array('uses' => 'ExpertAdviceController@mailEA'));
Route::get('/payment-expert-advice/{id}',array('uses' => 'ExpertAdviceController@paymentExpertAdvice'))->name('payment-expert-advice');
Route::post('/expert-payment-success',array('uses' => 'ExpertAdviceController@paymentSucess'));
Route::post('/expert-payment-success-indirect',array('uses' => 'ExpertAdviceController@paymentSucessIndirect'));
Route::get('/expert-payment-success/{id}',array('uses' => 'ExpertAdviceController@getPaymentSucess'));

#GST Registration
Route::get('/enquire-now',array('uses' => 'EnquiryNowController@enquiryNow'));
Route::post('/enquire-now-submit',array('uses' => 'EnquiryNowController@enquiryNowSubmit'));
