<?php
return [
    'home' => [
        'title' => "India's Best Consultancy | Online Udyam / Udyog Aadhar Registration For MSME, SSI ",
        'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
        'keywords' => 'Udyog Aadhaar Registration Online, Udyog Aadhaar Registration Fees,  Udyog Aadhaar Registration Fee, Udyog Aadhaar Registration, Msme Registration Fees, Msme Registration online, SSI Registration, SSI registration online',
    ],
    'udyam_registration' => [
        'title' => 'Online Application Form | Udyog Aadhar / MSME / SSI Registration ',
        'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
        'keywords' => 'Udyog Aadhar Form, Udyog Aadhar Form pdf, Udyog Aadhar Application Form, Udyog Aadhar Registration Form, How to register msme udyog aadhar',
    ],
    'msme_registration' => [
        'title' => 'Online Application Form | Updation of MSME / SSI Registration to Udyog Aadhar Registration',
        'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
        'keywords' => 'MSME Udyog Aadhar, Udyam Aadhar',
    ],
    'track_application' => [
        'title' => 'Application Status - MSME / Udyog Aadhar Registration',
        'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
        'keywords' => '',
    ],
    'benifits' => [
        'title' => 'Benefits | MSME / SSI / Udyog Aadhar Registration',
        'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
        'keywords' => 'Msme registration benefits',
    ],
    'faq' => [
        'title' => 'FAQ | MSME / SSI / Udyog Aadhar Registration ',
        'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
        'keywords' => 'FAQ',
    ],
    'about' => [
        'title' => 'About Us | MSME / SSI / Udyog Aadhar Registration',
        'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
        'keywords' => 'About Us',
    ],
    'contact' => [
        'title' => 'Contact Us | MSME / SSI / Udyog Aadhar Registration',
        'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
        'keywords' => 'Contact us',
    ],
    'procedure' => [
        'title' => 'Procedure | MSME / SSI / Udyog Aadhar Registration ',
        'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
        'keywords' => 'Msme registration process',
    ],
    'certificate' => [
        'title' => 'Sample MSME / Udyog Aadhar / SSI Certificate',
        'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
        'keywords' => 'Msme registration certificate sample, msme registration certificate, msme registration certificate format',
    ],
    'tnc' => [
	    'title' => 'T&C | MSME / SSI / Udyog Aadhar Registration',
	    'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
	    'keywords' => '',
    ],
    'privacy' => [
            'title' => 'Privacy Policy | MSME / SSI / Udyog Aadhar Registration',
            'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
            'keywords' => '',
    ],
    'refund'  => [
            'title' => 'Refund Policy | MSME / SSI / Udyog Aadhar Registration',
            'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
            'keywords' => '',
    ],
    'expert-advice'  => [
        'title' => 'Expert Advice | MSME / SSI / Udyog Aadhar Registration',
        'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
        'keywords' => '',
    ],
    'enquiry-now' => [
        'title' => 'Enquire Now | Best Tax Filing and Compliance Services in India',
        'description' => "India's Most Trusted Online Portal For Registration and Reliable assistance under UDYAM/MSME/SSI | Trusted By 5000+ MSME In India",
        'keywords' => '',
    ],
];
