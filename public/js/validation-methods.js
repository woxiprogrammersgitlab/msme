var retVal = true;

$.validator.addMethod('AadharNumber', function (value) {
    return /^\(?[\d\s]{4}-[\d\s]{4}-[\d\s]{4}$/.test(value);
}, 'The aadhar number may only contain numbers.');

$.validator.addMethod('AlphaSpace', function (value) {
    return /^[a-zA-Z\s]+$/.test(value);
}, 'This field may only contain letters, space.');

$.validator.addMethod('AlphaNumSpaceSpecial', function (value) {
    return /^[-@,.\/#&+\w\s]*$/.test(value);
}, 'This field may only contain letters, space and special char like - , @ . \ / & # ');

$.validator.addMethod('PanCardRule', function (value) {
    return /[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}$/.test(value);
}, 'The pan number is not valid.');

$.validator.addMethod('AlphaNumberSpace', function (value) {
    if(value != '')
        return /^[a-zA-Z0-9\s]+$/.test(value);
    else
        return true;
}, 'This field may only contain letters, numbers and space.');

$.validator.addMethod('AlphaNumber', function (value) {
    if(value != '')
        return /^[a-zA-Z0-9]+$/.test(value);
    else
        return true;
}, 'This field may only contain letters and numbers.');

$.validator.addMethod('MobileRule', function (value) {
    return /[7-9]{1}[0-9]{9}/.test(value);
}, 'Mobile number should be 10 digits number.');

$.validator.addMethod('IfscRule', function (value) {
    return /[a-zA-Z]{4}[0]{1}[0-9a-zA-Z]{6}/.test(value);
}, 'IFSC number is invalid.');

$.validator.addMethod('FloatNum', function (value) {
    if(/^\d{0,10}(\.\d{0,4}){0,1}$/.test(value)){
        return true;
    }
    return false;
}, 'Amount is invalid.');

$.validator.addMethod('filesize', function (value, element, param) {
    if(value == '') {
        return true;
    } else {
        if(typeof element.files === "undefined") {
            return true;
        } else if(value != '' && element.files[0].size <= param) {
            return true;
        } else {
            return false;
        }
    }

}, 'Filesize should be less than or equal to 5 MB');

$.validator.addMethod('GstRule', function (value) {
    if(value != '')
        return /[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[Z]{1}[0-9a-zA-Z]{1}$/.test(value);
    else
        return true;
}, 'GST number is invalid.');

$.validator.addMethod('UDMRule', function (value) {
    if(value != '')
        return /[UDM]{3}[0-9]+$/.test(value);
    else
        return true;
}, 'Application number is invalid.');

$.validator.addMethod("validateCaptcha", function(value, element)
{
    validateCap();
    return retVal;
}, 'Captcha value is invalid!');

function ajaxCallBack(retString){
    retVal = retString;
}

function validateCap() {
    var data = { "mathcaptcha" : $('#mathcaptcha').val() };

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        type: "POST",
        url: '/validate-captcha',
        data: data,
        success: function(data)
        {
            ajaxCallBack(true);
        },
        error: function(xhr, textStatus, errorThrown)
        {
            ajaxCallBack(false);
        }
    });
};

$( document ).ready(function() {

    $('#aadhar_number').keydown(function (e) {
        var key = e.charCode || e.keyCode || 0;
        $text = $(this);
        if (key !== 8 && key !== 9) {
            if ($text.val().length === 4) {
                $text.val($text.val() + '-');
            }
            if ($text.val().length === 9) {
                $text.val($text.val() + '-');
            }

        }

        return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
    });

    /*$('#existing_aadhar_number').keydown(function (e) {
        var key = e.charCode || e.keyCode || 0;
        $text = $(this);
        if (key !== 8 && key !== 9) {
            if ($text.val().length === 4) {
                $text.val($text.val() + '-');
            }
            if ($text.val().length === 9) {
                $text.val($text.val() + '-');
            }

        }

        return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
    });*/

    $( "#myForm" ).validate( {
        submitHandler : function(form, e) {
            //do something here
            e.preventDefault();
            $('#rzp-button-msme').attr("disabled", false);
            $('#cover-spin').show(0);
            var formData = $( "#myForm" ).serialize(); //Array 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url : "/udyam-msme-register-ajax",
                type: "POST",
                data : formData,
                success: function(data, textStatus, jqXHR)
                {
                    $("#myForm")[0].reset();
                    $('#cover-spin').hide(0);
                    var options = {
                        "key": data['key'], // Enter the Key ID generated from the Dashboard
                        "amount": data['amount'], // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                        "currency": data['currency'],
                        "name": data['name'],
                        "description": data['description'],
                        "image" : data['image'],
                        "order_id": data['order_id'], //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                        "handler": function (response){
                            var responseData = "shopping_order_id="+data['merchant_order_id']+"&razorpay_payment_id="+response.razorpay_payment_id+"&razorpay_order_id="+response.razorpay_order_id+"&razorpay_signature="+response.razorpay_signature;
                            $('#cover-spin').show(0);
                            $.ajax({
                                url : "/msme-payment-success-indirect",
                                type: "POST",
                                data : responseData,
                                success: function(data, textStatus, jqXHR)
                                {
                                  $('#cover-spin').hide(0);
                                  var base_url = window.location.origin;
                                  window.location.href = base_url + "/payment-success/" + data;
                                },
                                error: function (jqXHR, textStatus, errorThrown)
                                {
                            
                                }
                            });
                        },
                        "prefill": {
                            "name": data['cust_name'],
                            "email": data['email'],
                            "contact": data['contact']
                        },
                        "notes": {
                            "address": data['address'],
                            "merchant_order_id" : data['order_id'],
                        },
                        "theme": {
                            "color": "#5964C6"
                        }
                    };
                    var rzp1 = new Razorpay(options);
                    rzp1.open();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
            
                }
            });
        },
        rules: {
            aadhar_number: {
                required: true,
                AadharNumber:true,
            },
            existing_aadhar_number: {
                required: true,
                AlphaNumber:true,
                maxlength:20,
            },
            entrepreneur_name: {
                required: true,
                minlength: 5,
                maxlength: 255,
            },
            business_name: {
                required: true,
                minlength: 5,
                maxlength: 255,
            },
            organization_type_id: "required",
            pan_number: {
                required: true,
                PanCardRule: true
            },
            house_number: {
                required: true,
                maxlength: 255,
            },
            premise: {
                required: true,
                maxlength: 255,
            },
            road: {
                required: true,
                maxlength: 255,
            },
            area: {
                required: true,
                maxlength: 255,
            },
            city: {
                required: true,
                maxlength: 255,
                AlphaSpace: true,
            },
            pin: {
                required: true,
                number: true,
                maxlength: 6,
            },
            state_id: "required",
            district: {
                required: true,
                maxlength: 255,
                AlphaSpace: true,
            },
            is_office_address_same: "required",
            house_number1: {
                required: function(element){
                    return $("#is_office_address_same").val()=="no";
                },
                AlphaNumSpaceSpecial: true,
                maxlength: 255,
            },
            premise1: {
                required: function(element){
                    return $("#is_office_address_same").val()=="no";
                },
                AlphaNumSpaceSpecial: true,
                maxlength: 255,
            },
            road1: {
                required: function(element){
                    return $("#is_office_address_same").val()=="no";
                },
                AlphaNumSpaceSpecial: true,
                maxlength: 255,
            },
            area1: {
                required: function(element){
                    return $("#is_office_address_same").val()=="no";
                },
                AlphaNumSpaceSpecial: true,
                maxlength: 255,
            },
            city1: {
                required: function(element){
                    return $("#is_office_address_same").val()=="no";
                },
                AlphaSpace: true,
                maxlength: 255,
            },
            pin1: {
                required: function(element){
                    return $("#is_office_address_same").val()=="no";
                },
                number: true,
                maxlength: 6,
            },
            state_id1: {
                required: function(element){
                    return $("#is_office_address_same").val()=="no";
                }
            },
            district1: {
                required: function(element){
                    return $("#is_office_address_same").val()=="no";
                },
                AlphaNumSpaceSpecial: true,
                maxlength: 255,
            },
            mobile: {
                required: true,
                MobileRule: true
            },
            email: {
                required: true,
                email: true
            },
            commencement_date: "required",
            bank_account_number: {
                required: true,
                minlength: 6,
                maxlength: 20,
                number: true,
            },
            ifsc_code: {
                required: true,
                IfscRule: true,
            },
            business_activity_type: {
                required: true,
            },
            nic_code_id: {
                required: function(element){
                    return $("#business_activity_type").val()=="Manufacturer";
                },
            },
            nic_code_id1: {
                required: function(element){
                    return $("#business_activity_type").val()=="Service provider";
                },
            },
            about_business: {
                required: false,
                AlphaNumberSpace: function(element){
                    return $("#about_business").val()!="";
                },
                maxlength: 255,
            },
            employee_count: {
                required: true,
                number: true,
            },
            investment_in_plant: {
                required: true,
                maxlength: 20
            },
            aadhar_image: {
                required: false,
                accept:"application/pdf,image/jpeg,image/jpg,image/png",
                filesize: 5000000, //5 MB
            },
            aadhar_image_existing: {
                required: false,
                accept:"application/pdf,image/jpeg,image/jpg,image/png",
                filesize: 5000000, //5 MB
            },
            gstin_number: {
                required: false,
                GstRule: true
            },
            mathcaptcha: {
                required: false,
                number: true,
                validateCaptcha: true
            },
        },
        messages: {
            aadhar_number: {
                required: "Aadhar number is required",
            },
            business_activity_type: {
                required: "Business activity type is required",
            },
            entrepreneur_name: {
                required: "Entrepreneur name is required",
                minlength: "Entrepreneur name must be at least 5 characters long",
                maxlength: "Entrepreneur name can be 255 characters long",
                AlphaSpace: "Entrepreneur name may only contain letters, space."
            },
            business_name: {
                required: "Business name is required",
                minlength: "Business name must be at least 5 characters long",
                maxlength: "Business name can be 255 characters long",
                AlphaSpace: "Business name may only contain letters, space."
            },
            organization_type_id: {
                required: "Organization type is required"
            },
            pan_number: {
                required: "Pan number is required"
            },
            bank_account_number: {
                required: "Bank account number is required",
                minlength: "Bank account number must be at least 10 characters long",
                maxlength: "Bank account number can be 20 characters long",
                number: "Bank account number may only contain numbers."
            },
            aadhar_image: {
                accept: "File should be: jpg, png or pdf format.",
            },
            gstin_number: {
                required: "GSTIN number is required",
            },
            t_and_c: "Please accept our terms & condition"
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        success: function ( label, element ) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if ( !$( element ).next( "span" )[ 0 ] ) {
                $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
            }
        },
        // highlight: function ( element, errorClass, validClass ) {
        // 	$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
        // 	$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
        // },
        // unhighlight: function ( element, errorClass, validClass ) {
        // 	$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
        // 	$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
        // }
    });

    $( "#contact_form" ).validate( {
        submitHandler : function(form) {
            //do something here
            form.submit();
        },
        rules: {
            name: {
                required: true,
                AlphaSpace:true,
            },
            email: {
                required: true,
                email:true,
            },
            mobile: {
                required: true,
                MobileRule: true
            },
            message: {
                required: false
            },
            mathcaptcha: {
                required: false,
                number: true,
                validateCaptcha: true
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        success: function ( label, element ) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if ( !$( element ).next( "span" )[ 0 ] ) {
                $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
            }
        },
    });

    $( "#track-form" ).validate( {
        submitHandler : function(form) {
            //do something here
            form.submit();
        },
        rules: {
            order_number: {
                required: true,
                UDMRule:true,
            },
            mobile: {
                required: true,
                MobileRule: true
            },
            mathcaptcha: {
                required: false,
                number: true,
                validateCaptcha: true
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        success: function ( label, element ) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if ( !$( element ).next( "span" )[ 0 ] ) {
                $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
            }
        },
    });

});
