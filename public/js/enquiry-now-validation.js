$( document ).ready(function() {
	$( "#enquiry_now" ).validate( {
        submitHandler : function(form, e) {
            e.preventDefault();
            $('#enq_now_submit_btn_id').attr("disabled", false);
            $('#cover-spin').show(0);
            var formData = $( "#enquiry_now" ).serialize(); //Array 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url : "/enquire-now-submit",
                type: "POST",
                data : formData,
                success: function(data, textStatus, jqXHR)
                {
                    $("#enquiry_now")[0].reset();
                    $('#cover-spin').hide(0);
                    alert(data);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
            
                }
            });
            
            //form.submit();
        },
        rules: {
            customer_name: {
                required: true,
                minlength: 5,
                maxlength: 255,
                AlphaSpace: true,
            },
            mobile_number: {
                required: true,
                MobileRule: true,
                maxlength: 10,
            },
            customer_email: {
                required: true,
                email: true
            },
            enquiry_type_id: "required",
            other_issue: {
                required: true,
                minlength: 5,
                maxlength: 255,
            },
            customer_message: {
                required: true,
                minlength: 5,
                maxlength: 1024,
            },
            preferable_slot: "required",
            t_and_c: "required",
            mathcaptcha: {
                required: false,
                number: true,
                validateCaptcha: true
            },
        },
        messages: {
            customer_name: {
                required: "Customer name is required",
                minlength: "Customer name must be at least 5 characters long",
                maxlength: "Customer name can be 255 characters long",
                AlphaSpace: "Customer name may only contain letters, space."
            },
            enquiry_type_id : {
                required: "Please select enquiry type"
            },
            mobile_number: {
                required: "Mobile number is required",
                maxlength: "Mobile number  can be 10 characters long",
            },
            preferable_slot: {
                required: "Please select preferable time slot"
            },
            other_issue: {
                required: "This field is required.",
            },
            customer_message: {
                required: "Customer message is required",
            },
            t_and_c: "Please accept our terms & condition"
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        success: function ( label, element ) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if ( !$( element ).next( "span" )[ 0 ] ) {
                $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
            }
        },
    });



});
