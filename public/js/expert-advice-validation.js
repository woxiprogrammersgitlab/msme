$( document ).ready(function() {
	$( "#expert_advice" ).validate( {
        submitHandler : function(form, e) {
            e.preventDefault();
            $('#rzp-button1').attr("disabled", false);
            $('#cover-spin').show(0);
            var formData = $( "#expert_advice" ).serialize(); //Array 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url : "/expert-advice-register",
                type: "POST",
                data : formData,
                success: function(data, textStatus, jqXHR)
                {
                    $("#expert_advice")[0].reset();
                    $('#cover-spin').hide(0);
                    var options = {
                        "key": data['key'], // Enter the Key ID generated from the Dashboard
                        "amount": data['amount'], // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                        "currency": data['currency'],
                        "name": data['name'],
                        "description": data['description'],
                        "image" : data['image'],
                        "order_id": data['order_id'], //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                        "handler": function (response){
                            var responseData = "shopping_order_id="+data['merchant_order_id']+"&razorpay_payment_id="+response.razorpay_payment_id+"&razorpay_order_id="+response.razorpay_order_id+"&razorpay_signature="+response.razorpay_signature;
                            $('#cover-spin').show(0);
                            $.ajax({
                                url : "/expert-payment-success-indirect",
                                type: "POST",
                                data : responseData,
                                success: function(data, textStatus, jqXHR)
                                {
                                  $('#cover-spin').hide(0);
                                  var base_url = window.location.origin;
                                  window.location.href = base_url + "/expert-payment-success/" + data;
                                },
                                error: function (jqXHR, textStatus, errorThrown)
                                {
                            
                                }
                            });
                        },
                        "prefill": {
                            "name": data['cust_name'],
                            "email": data['email'],
                            "contact": data['contact']
                        },
                        "notes": {
                            "address": data['address'],
                            "merchant_order_id" : data['order_id'],
                        },
                        "theme": {
                            "color": "#5964C6"
                        }
                    };
                    var rzp1 = new Razorpay(options);
                    rzp1.open();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
            
                }
            });
            
            //form.submit();
        },
        rules: {
            customer_name: {
                required: true,
                minlength: 5,
                maxlength: 255,
                AlphaSpace: true,
            },
            mobile_number: {
                required: true,
                MobileRule: true,
                maxlength: 10,
            },
            customer_email: {
                required: true,
                email: true
            },
            issue_related_to_id: "required",
            other_issue: {
                required: true,
                minlength: 5,
                maxlength: 255,
            },
            customer_message: {
                required: true,
                minlength: 5,
                maxlength: 1024,
            },
            preferable_slot: "required",
            t_and_c: "required",
            mathcaptcha: {
                required: false,
                number: true,
                validateCaptcha: true
            },
        },
        messages: {
            customer_name: {
                required: "Customer name is required",
                minlength: "Customer name must be at least 5 characters long",
                maxlength: "Customer name can be 255 characters long",
                AlphaSpace: "Customer name may only contain letters, space."
            },
            issue_related_to_id : {
                required: "Please select issue related to"
            },
            mobile_number: {
                required: "Mobile number is required",
                maxlength: "Mobile number  can be 10 characters long",
            },
            preferable_slot: {
                required: "Please select preferable time slot"
            },
            other_issue: {
                required: "This field is required.",
            },
            customer_message: {
                required: "Customer message is required",
            },
            t_and_c: "Please accept our terms & condition"
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        },
        success: function ( label, element ) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if ( !$( element ).next( "span" )[ 0 ] ) {
                $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
            }
        },
    });



});
