@extends('layouts.master')
@section('content')



<section class="section" id="services-2">

        <div class="container">

            <div class="row justify-content-center">

                <div class="col-md-8 col-lg-6 text-center">

                    <div class="section-heading">

                        <!-- Heading -->

                        <h2 class="section-title mb-2  text-white">

                            Benefits of Udyam/MSME Registration In India

                        </h2>



                        <!-- Subheading -->

                        <p class="mb-5 text-white" style="width: 100%;">

                            The Indian government has always been in favour of providing benefits to Micro, small and medium enterprises (MSMEs). There are many advantages of obtaining Udyam registration in India, which can be only availed if the business had registered itself as an MSME/SSI under MSME Act. The following are a few advantages or obtaining Udyam/SSI/MSME registration in India:

                        </p>

                    </div>

                </div>

            </div> <!-- / .row -->



            <div class="row">

                <div class="col-lg-4 col-sm-6 col-md-6 mb-30">

                    <div class="web-service-block">

                        <h3>Industrial Promotion Subsidy Eligibility</h3>

                        <p>Enterprises registered under MSME are also eligible for a subsidy for Industrial Promotion as suggested by the Government.</p>

                    </div>

                </div>

                <div class="col-lg-4 col-sm-6 col-md-6 mb-30">

                    <div class="web-service-block">

                        <h3>Subsidy on Patent Registration</h3>

                        <p>A hefty 50% subsidy is given to the Enterprise that has the certificate of registration granted by MSME. This subsidy can be availed for patent registration by giving application to respective ministry.</p>

                    </div>

                </div>

                <div class="col-lg-4 col-sm-6 col-md-6 mb-30">

                    <div class="web-service-block">

                        <h3>Overdraft Interest Rate Exemption</h3>

                        <p>Businesses or enterprises registered under MSME/Udyam can avail a benefit of 1% on the Over Draft as mentioned in a scheme that differs from bank to bank.</p>

                    </div>

                </div>



                <div class="col-lg-4 col-sm-6 col-md-6 ">

                    <div class="web-service-block">

                        <h3>Bank Loans (Collateral Free)</h3>

                        <p>The Government of India has made collateral-free credit available to all small and micro business sectors. This initiative guarantees funds to micro and small sector enterprises. Under this scheme, both the old as well as the new enterprises can claim the benefits. A trust named The Credit Guarantee Trust Fund Scheme was introduced by the GOI(Government Of India), SIDBI(Small Industries Development Bank Of India) and the Ministry of Micro, Small and Medium Enterprise to make sure this scheme is implemented (Credit Guarantee Scheme) for all Micro and Small Enterprise.</p>

                    </div>

                </div>

                <div class="col-lg-4 col-sm-6 col-md-6 ">

                    <div class="web-service-block"> 

                        <h3>Fewer Electricity Bills</h3>

                        <p>This concession is available to all the Enterprises that have the MSME Registration Certificate by providing an application to the department of the electricity along with the certificate of registration by MSME.</p>

                    </div>

                </div>

                <div class="col-lg-4 col-sm-6 col-md-6 ">

                    <div class="web-service-block">

                        <h3>ISO Certification Charges Reimbursement</h3>

                        <p>The registered MSME enterprises can claim the reimbursement of the expenses that were spent for the ISO certification.</p>

                    </div>

                </div>

                <div class="col-lg-12 col-sm-12 col-md-12 ">

                    <div class="web-service-block">

                        <h3>Protection against Payments (Delayed Payments)</h3>

                        <ul>

                            <li>

                                <p>At times, the buyers of services or products from the MSME’s or SSIs tend to delay the payment. The Ministry of Micro, Small and Medium Enterprise lend a helping hand to such enterprises by giving them the right to collect interest on the payments that are delayed from the buyer’s side. The settlement of such disputes must be done in minimum time through conciliation and arbitration.</p>

                            </li>

                            <li>

                                <p>In case, if any MSME registered enterprise supplies any goods or services to a buyer then the buyer is required to make the payment on or before the agreed date of payment or within 15 days from the day they had accepted the goods and services from MSME or SCI registered business( if there is no mention of the date of payment).</p>

                            </li>

                            <li>

                                <p>If the buyer delays the payment for more than 45 days after accepting the products or services then the buyer has to pay compound interest along with interests (monthly) on the amount that was agreed to be paid. The interest rate is three times the rate that is notified by the Reserve Bank of India.</p></p>

                            </li>

                        </ul>                        

                    </div>

                </div>

            </div>

            

        </div>

    </section>

@endsection
@section('js')
<script>

</script>
@endsection

