<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style type="text/css" rel="stylesheet" media="all">
        /* Media Queries */
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>

<?php
$style = [
    /* Layout ------------------------------ */
        'body' => 'margin: 0; padding: 0; width: 100%; background-color: #5964C6;',
        'email-wrapper' => 'width: 100%; margin: 0; padding: 0; background-color: #5964C6;',
    /* table ----------------------- */
        'master-table' => 'border:1px solid #5964C6;',
    /* Masthead ----------------------- */
        'email-masthead' => 'padding: 25px 0; text-align: center;',
        'email-masthead_name' => 'font-size: 24px; font-weight: bold; color: #FFFFFF; text-decoration: none; text-shadow: 0 1px 0 white;',
        'email-body' => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;',
        'email-body_inner' => 'width: 100%; margin: 0 auto; padding: 0;',
        'email-body_cell' => 'padding: 15px;text-align: left;',
        'email-footer' => 'width: 100%; mrgin: 0 auto; padding: 0; text-align: center;',
        'email-footer_cell' => 'color: #000; padding: 35px; text-align: center;',
    /* Body ------------------------------ */
        'body_action' => 'width: 100%; margin: 30px auto; padding: 0; text-align: center;',
        'body_sub' => 'margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;',
    /* Type ------------------------------ */
        'anchor' => 'color: #FFFFFF;',
        'header-1' => 'margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;',
        'paragraph' => 'margin-top: 0; color: #000; font-size: 16px; line-height: 1.5em;text-align: left;',
        'paragraph-dis' => 'margin-top: 0; color: #74787E; font-size: 10px; line-height: 1.5em;text-align: left;',
        'paragraph-sub' => 'margin-top: 0; color: #000; font-size: 12px; line-height: 1.5em;',
        'paragraph-center' => 'text-align: center;',
    /* Buttons ------------------------------ */
        'button' => 'display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
                 background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                 text-align: center; text-decoration: none; -webkit-text-size-adjust: none;',
        'button--green' => 'background-color: #22BC66;',
        'button--red' => 'background-color: #dc4d2f;',
        'button--blue' => 'background-color: #3869D4;',
        'button-primary'=> 'text-transform: uppercase;background-color: #5964C6;border-color: #5964C6;color: #ffffff;outline: none !important;vertical-align: middle;    padding: 0.8rem 1rem;font-size: 0.875rem;line-height: 1.5;border-radius: 0.2rem;text-decoration: none;',
        'strong-color' => 'color:#5964c6;',
];
?>

<?php $fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;'; $actionColor = 'button--blue';?>

<body style="{{ $style['body'] }}">
<table width="100%" cellpadding="0" cellspacing="0" style="{{ $style['master-table'] }}">
    <tr>
        <td style="{{ $style['email-wrapper'] }}" align="center">
            <table width="100%" cellpadding="0" cellspacing="0" >
                <!-- Logo -->
                <tr>
                    <td style="{{ $style['email-masthead'] }}">
                        <a style="{{ $fontFamily }} {{ $style['email-masthead_name'] }}" href="{{ url('/') }}" target="_blank">
                            {{ config('app.name') }}
                        </a>
                    </td>
                </tr>

                <!-- Email Body -->
                <tr>
                    <td style="{{ $style['email-body'] }}" width="100%">
                        <table style="{{ $style['email-body_inner'] }}" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="{{ $fontFamily }} {{ $style['email-body_cell'] }}">
                                    <!-- Greeting -->
                                    <h1 style="{{ $style['header-1'] }}">
                                        Dear <strong style="{{ $style['strong-color'] }}">{{$custRegData->customer_name}}</strong>,
                                    </h1>
                                </td>
                            </tr>
                            <tr>
                                <td style="{{ $fontFamily }} {{ $style['email-body_cell'] }}">
                                    <!-- Intro -->

                                    <p style="{{ $style['paragraph'] }}">
                                        Thank you for showing interest in our Expert Call back service. We love to help you from where you are. Your request no is <strong style="{{ $style['strong-color'] }}">{{$custRegData->expert_advice_cust_reg_number}}</strong>. 
                                        Please complete the payment for receiving a call from our Expert. <br/><br/><br/>
                                        <a href="{{$applicationPaymentUrl}}" target="_blank"><span style="font-size:20px;padding:10px;color:#fff;background-color:#5964c6;border:1px solid #fff;">Pay Now</span></a>
                                    </p>
                                    <br/>
                                    <!--<p style="font-size:12px;">
                                        This is system generated mail. Please do not reply to msmeudyamonline@gmail.com.
                                    </p>-->
                                </td>
                            </tr>
                            <tr>
                                <td style="{{ $fontFamily }} {{ $style['email-body_cell'] }}">

                                    <p style="{{ $style['paragraph-dis'] }}">
                                        <strong>Confidentiality Warning:</strong> This message and any attachments are intended only for the use of the intended recipient(s), are confidential and may be privileged. 
                                        If you are not the intended recipient, you are hereby notified that any review, re-transmission, conversion to hard copy, copying, circulation or other use of this message and any attachments is strictly prohibited. 
                                        If you are not the intended recipient, please notify the sender immediately by return email and delete this message and any attachments from your system. 
                                        This message and any attachments are intended only for the use of the intended recipient(s), are confidential and may be privileged. 
                                        If you are not the intended recipient, you are hereby notified that any review, re-transmission, conversion to hard copy, copying, circulation or other use of this message and any attachments is strictly prohibited. 
                                        If you are not the intended recipient, please notify the sender immediately by return email and delete this message and any attachments from your system.
                                    </p>

                                    <p style="{{ $style['paragraph-dis'] }}">
                                        <strong>Virus Warning:</strong> Although the company has taken reasonable precautions to ensure no viruses are present in this email. 
                                        The company cannot accept responsibility for any loss or damage arising from the use of this email or attachment.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <!-- Footer -->
                <tr>
                    <td>
                        <table style="{{ $style['email-footer'] }}" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="{{ $fontFamily }} {{ $style['email-footer_cell'] }}">
                                    <p style="{{ $style['paragraph-sub'] }}">
                                        &copy; {{ date('Y') }}
                                        <a style="{{ $style['anchor'] }}" href="{{ url('/') }}" target="_blank">{{ config('app.name') }}</a>.
                                        All rights reserved.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>

