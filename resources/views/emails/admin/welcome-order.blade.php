<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style type="text/css" rel="stylesheet" media="all">
        /* Media Queries */
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
    <style>
        table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }

        td, th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 8px;
        }

        tr:nth-child(even) {
          background-color: #dddddd;
        }
        </style>
</head>

<?php
$style = [
    /* Layout ------------------------------ */
        'body' => 'margin: 0; padding: 0; width: 100%; background-color: #5964C6;',
        'email-wrapper' => 'width: 100%; margin: 0; padding: 0; background-color: #5964C6;',
    /* table ----------------------- */
        'master-table' => 'border:1px solid #5964C6;',
    /* Masthead ----------------------- */
        'email-masthead' => 'padding: 25px 0; text-align: center;',
        'email-masthead_name' => 'font-size: 24px; font-weight: bold; color: #FFFFFF; text-decoration: none; text-shadow: 0 1px 0 white;',
        'email-body' => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;',
        'email-body_inner' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0;',
        'email-body_cell' => 'padding: 35px;text-align: center;',
        'email-footer' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;',
        'email-footer_cell' => 'color: #000; padding: 35px; text-align: center;',
    /* Body ------------------------------ */
        'body_action' => 'width: 100%; margin: 30px auto; padding: 0; text-align: center;',
        'body_sub' => 'margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;',
    /* Type ------------------------------ */
        'anchor' => 'color: #FFFFFF;',
        'header-1' => 'margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: center;',
        'paragraph' => 'margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;text-align: center;',
        'paragraph-sub' => 'margin-top: 0; color: #000; font-size: 12px; line-height: 1.5em;',
        'paragraph-center' => 'text-align: center;',
    /* Buttons ------------------------------ */
        'button' => 'display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
                 background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                 text-align: center; text-decoration: none; -webkit-text-size-adjust: none;',
        'button--green' => 'background-color: #22BC66;',
        'button--red' => 'background-color: #dc4d2f;',
        'button--blue' => 'background-color: #3869D4;',
        'button-primary'=> 'text-transform: uppercase;background-color: #5964C6;border-color: #5964C6;color: #ffffff;outline: none !important;vertical-align: middle;    padding: 0.8rem 1rem;font-size: 0.875rem;line-height: 1.5;border-radius: 0.2rem;text-decoration: none;',
];
?>

<?php $fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;'; $actionColor = 'button--blue';?>

<body style="{{ $style['body'] }}">
<table width="100%" cellpadding="0" cellspacing="0" style="{{ $style['master-table'] }}">
    <tr>
        <td style="{{ $style['email-wrapper'] }}" align="center">
            <table width="100%" cellpadding="0" cellspacing="0" >
                <!-- Logo -->
                <tr>
                    <td style="{{ $style['email-masthead'] }}">
                        <a style="{{ $fontFamily }} {{ $style['email-masthead_name'] }}" href="{{ url('/') }}" target="_blank">
                            {{ config('app.name') }}
                        </a>
                    </td>
                </tr>

                <!-- Email Body -->
                <tr>
                    <td style="{{ $style['email-body'] }}" width="100%">
                        <table style="{{ $style['email-body_inner'] }}" align="center"  cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="{{ $fontFamily }} {{ $style['email-body_cell'] }}">
                                    <!-- Greeting -->
                                    <h1 style="{{ $style['header-1'] }}">
                                        Dear Admin,
                                    </h1>

                                    <!-- Intro -->

                                    <p style="{{ $style['paragraph'] }}">
                                        Check New Registration Entry
                                    </p>
                                    <p style="{{ $style['paragraph'] }}">
                                        <table>
                                            <tr>
                                              <th>Name</th>
                                              <th>Data</th>
                                            </tr>
                                            <tr>
                                              <td>Existing Adhar Number</td>
                                              <td>{{$aadharData->existing_aadhar_number}}</td>
                                            </tr>
                                            <tr>
                                                <td>Aadhar Number</td>
                                                <td>{{$aadharData->aadhar_number}}</td>
                                            </tr>
                                            <tr>
                                                <td>Entrepreneur Name</td>
                                                <td>{{$aadharData->entrepreneur_name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Social Category</td>
                                                <td>{{$aadharData->socialCategory->name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Gender</td>
                                                <td>{{$aadharData->gender}}</td>
                                            </tr>
                                            <tr>
                                                <td>Is Handicapped?</td>
                                                <td>@if($aadharData->is_handicapped) YES @else NO @endif</td>
                                            </tr>
                                            <tr>
                                                <td>Business Name</td>
                                                <td>{{$aadharData->business_name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Organization Type</td>
                                                <td>{{$aadharData->organizationType->name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Pan Number</td>
                                                <td>{{$aadharData->pan_number}}</td>
                                            </tr>
                                            <tr>
                                                <td>Mobile</td>
                                                <td>+91-{{$aadharData->mobile}}</td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td>{{$aadharData->email}}</td>
                                            </tr>
                                            <tr>
                                                <td>Commencement Date</td>
                                                <td>{{$aadharData->commencement_date}}</td>
                                            </tr>
                                            <tr>
                                                <td>Bank Account Number</td>
                                                <td>{{$aadharData->bank_account_number}}</td>
                                            </tr>
                                            <tr>
                                                <td>Ifsc Code</td>
                                                <td>{{$aadharData->ifsc_code}}</td>
                                            </tr>
                                            <tr>
                                                <td>Business Activity Type</td>
                                                <td>{{$aadharData->business_activity_type}}</td>
                                            </tr>
                                            <tr>
                                                <td>Nic Code</td>
                                                <td>{{$aadharData->nicCode->name}}</td>
                                            </tr>

                                            <tr>
                                                <td>About Business</td>
                                                <td>{{$aadharData->about_business}}</td>
                                            </tr>
                                            <tr>
                                                <td>Employee Count</td>
                                                <td>{{$aadharData->employee_count}}</td>
                                            </tr>
                                            <tr>
                                                <td>Investment in Plant</td>
                                                <td>{{$aadharData->investment_in_plant}}</td>
                                            </tr>
                                            <tr>
                                                <td>Gstin Number</td>
                                                <td>{{$aadharData->gstin_number}}</td>
                                            </tr>
                                            <tr>
                                                <td>Interested to get registered on Government e-Market(GeM) Portal?</td>
                                                <td>@if($aadharData->is_gem) YES @else NO @endif</td>
                                            </tr>
                                            <tr>
                                                <td>Interested to get registered on TReDS Portal?</td>
                                                <td>@if($aadharData->is_treds) YES @else NO @endif</td>
                                            </tr>
                                            <tr>
                                                <td>Payment Status</td>
                                                <td>@if($aadharData->status) Completed @else Pending @endif</td>
                                            </tr>
                                            @php $index = 1; @endphp
                                            @foreach($aadharData->address as $address)
                                                @if(count($aadharData->address) == 2)
                                                    @if($index == 1)
                                                        <tr>
                                                            <td>Address Type</td>
                                                            <td>Home</td>
                                                        </tr>
                                                        @php $index++; @endphp
                                                    @elseif($index == 2)
                                                        <tr>
                                                            <td>Address Type</td>
                                                            <td>Office</td>
                                                        </tr>
                                                    @endif
                                                <tr>
                                                    <td>House Number</td>
                                                    <td>{{$address->house_number}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Premise</td>
                                                    <td>{{$address->premise}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Road</td>
                                                    <td>{{$address->road}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Area</td>
                                                    <td>{{$address->area}}</td>
                                                </tr>
                                                <tr>
                                                    <td>City</td>
                                                    <td>{{$address->city}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Pincode</td>
                                                    <td>{{$address->pin}}</td>
                                                </tr>
                                                <tr>
                                                    <td>State</td>
                                                    <td>{{$address->state->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td>District</td>
                                                    <td>{{$address->district}}</td>
                                                </tr>
                                                @else
                                                    <tr>
                                                        <td>Address Type</td>
                                                        <td>Home</td>
                                                    </tr>
                                                    <tr>
                                                        <td>House Number</td>
                                                        <td>{{$address->house_number}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Premise</td>
                                                        <td>{{$address->premise}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Road</td>
                                                        <td>{{$address->road}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Area</td>
                                                        <td>{{$address->area}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>City</td>
                                                        <td>{{$address->city}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pincode</td>
                                                        <td>{{$address->pin}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>State</td>
                                                        <td>{{$address->state->name}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>District</td>
                                                        <td>{{$address->district}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Is office Address Same</td>
                                                        <td>Yes</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                          </table>
                                    </p>


                                    <!-- Salutation -->
                                    <p style="{{ $style['paragraph'] }}">
                                        DISCLAIMER ============================================================
                                        This e-mail may contain privileged and confidential information.  It is intended only for the use of the individual or entity to which it is addressed. If you are not the intended recipient, you are not authorized to read, retain, copy, print, distribute or use this message. If you have received this communication in error, please notify the sender and delete all copies of this message. We do not accept any liability for virus infected mails.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <!-- Footer -->
                <tr>
                    <td>
                        <table style="{{ $style['email-footer'] }}" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="{{ $fontFamily }} {{ $style['email-footer_cell'] }}">
                                    <p style="{{ $style['paragraph-sub'] }}">
                                        &copy; {{ date('Y') }}
                                        <a style="{{ $style['anchor'] }}" href="{{ url('/') }}" target="_blank">{{ config('app.name') }}</a>.
                                        All rights reserved.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
