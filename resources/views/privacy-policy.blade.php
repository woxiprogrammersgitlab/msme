@extends('layouts.master')
@section('content')

<section class="section" id="contact">

        <div class="container">

            <div class="row">

                <div class="col-lg-12" style="padding: 20px">

                    <h2 style="color: orange;">PRIVACY POLICY</h2>

                    <p style="color: #5964C6;">We are sensitive to privacy issues and have established this Policy so that you can understand the care with which we intend to treat your Personal Information. This privacy policy should be read in conjunction with our Terms and conditions. We, at Udyamonline.org, respect your privacy and the confidentiality of your Personal Information (including email address, phone numbers and mail address etc. Only authorized personnel/employees of Udyamonline.org have access to your Personal Information specifically permitted by you for our use by us or for any other purpose. Our company's resources and entire staff are fully committed to safeguarding your personal data i.e. email address phone numbers, and mail address etc. We provide highest security and other precautionary measures are in place to protect your non-public information. We only collect, store and use your personal information (including email address, phone numbers and mail address etc) for defined purposes. We are committed to protecting your privacy. We use the information we collect on the Udyamonline.org to enhance your overall experience. We do not sell, trade, or rent your personal information to others. By using our webiste, you consent to the collection and use of this information by our website. If we decide to change our privacy policy, we will post those changes on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.</p>

                    <h2 style="color: orange;">CHANGES TO THIS PRIVACY POLICY</h2>

                    <p style="color: #5964C6;">Udyamonline.org have the discretion to update this privacy policy at any time. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>

                    <h2 style="color: orange;">Security Measures</h2>

                    <p style="color: #5964C6;">At Udyamonline.org, we use the industry-standard Secured Socket Layer (SSL) protocol which provides data encryption, message integrity, and server authentication when your data is sent to us. When you see a small lock at the bottom right or left corner of your browser window, you know your data is being encrypted for transmission. All information (including email address) you enter while using Udyamonline.org is used to provide the services requested by you. We maintain physical, electronic, and procedural safeguards to protect your personal information (including email address and phone numbers). All data is stored on Udyamonline.org servers and is backed up to prevent the loss of data. Access to (and the use thereof) personally identifiable customer information (including email address and phone numbers) is restricted to authorized personnel as dictated by service, support, and / or fulfilment requirements.</p>

                    <h2 style="color: orange;">COLLECTION OF PERSONAL AND OTHER INFORMATION</h2>

                    <ol>

                        <li><p>It is being accepted by the User that the personal information is collected and stored by the company, which is provided by the User from time to time on Udyamonline.org but not limited to the User’s username, passwords, email address, name, address, age, date of birth, sex, nationality, shopping preferences, browsing history, logo etc., as well as any images or other information uploaded/published by the User on the Udyamonline.org. The Udyamonline.org uses this information which is given by the User to provide the services. The User’s need is fulfilled accordingly. This information is used by the Udyamonline.org to improve the portal to make it safer and easier for the User.</p></li>    

                        <li><p>There is certain information which are tracked automatically by the Udyamonline.org about the User like User’s IP address and User’s behavior on Udyamonline.org. This information can be used by Udyamonline.org to do internal research on user demographics, interests, and behavior, to enable the Udyamonline.org to better understand. The User knows that such information may include the URL that the User visited prior to accessing the Udyamonline.org, the URL which the User subsequently visits (whether or not these URLs form a part of the Udyamonline.org), the User’s computer & web browser information, the User’s IP address, etc.</p></li>

                        <li><p>If any User chooses to buy any product or services from the Udyamonline.org, the User is allowing the Udyamonline.org to gather the personal information of the User.</p></li>

                        <li><p>The User is aware that if any information which is provided by User whether it is posted message/ review / feedback anywhere on Udyamonline.org can be used by the Udyamonline.org to help out the User in every possible way in future. This retained information can be used to resolve disputes, provide customer support, troubleshoot problems and much other and that such information, if requested, may be provided to judicial or governmental authorities of requisite jurisdiction, or otherwise used by the Udyamonline.org as permitted by applicable laws.</p></li>

                        <li><p>All the information whether it is personal or any other like emails or letters or any feedback from other users or third party regarding the User’s activities or posting on Udyamonline.org is collected or gathered by the company and is compiled in a file or folder by the website. The User is aware of all this information.</p></li>

                        <li><p>A User/Users can browse a Udyamonline.org without providing any information to the website but for certain activities and area, the User has to provide certain information for registration such as placing an order. The User is aware that the information provided to the Udyamonline.org can be used to send the User offers and promotions whether or not based on the User’s previous orders and interests, and the User hereby expressly consents to receive the same.</p></li>

                        <li><p>There are certain online surveys which are optional and Udyamonline.org can request the User to complete those surveys. Sometimes these surveys may ask the User to provide the contact information and demographic information (like zip code, age, income bracket, sex, etc.). This data is used to customize the Udyamonline.org for the benefit of the User, and providing all users of the Udyamonline.org with products/services/content that the Company/Udyamonline.org believes they might be interested in availing of, and also to display content according to the User’s preferences, the user is aware about all this.</p></li>

                        <li><p>The Udyamonline.org can ask or request the User to provide the reviews for the products/services which are purchased/ availed by the User from Udyamonline.org. The User is aware that such reviews will help other users of the Udyamonline.org make prudent and correct purchases, and also help the Udyamonline.org remove sellers whose products are unsatisfactory in any way, and the User hereby expressly authorizes the Udyamonline.org to publish any and all reviews written by the User on the Udyamonline.org, along with the User’s name and certain contact details, for the benefit and use of other Users of the Udyamonline.org.</p></li>

                        <li><p>The information like content/reviews/surveys/feedback which is submitted by the User to Udyamonline.org may or may not be stored, uploaded, published or displayed in any manner. The User authorizes the Udyamonline.org/Company to remove any such content, review, survey or feedback from Udyamonline.org which has been submitted by User, without cause or being required to notify the User of the same.</p></li>

                    </ol>

                    <h2 style="color: orange;">COOKIES</h2>

                    <ol>

                        <li><p>A cookie is an information which is being stored on hard disk by Udyamonline.org so that it can remember something about the User later time. The User is aware that this information that is Cookie is stored by the Udyamonline.org/Company to use it later if required and that cookies are useful for enabling the browser to remember information specific to a given user, including but not limited to a User’s login identification, password, etc. Web site installs permanent and temporary cookies in User’s hard disk and browser and the User is aware of it and does hereby expressly consent to the same.</p></li>

                        <li><p>There are many data collection devices such as cookies on certain pages of the Udyamonline.org. These devices help in analyzing the web page flow, measure promotional effectiveness and promote trust and safety. These are the features which are only available due to the use of cookies. The User is aware of all this. While the User is free to decline the website cookies if the User’s browser permits, the User may consequently be unable to use certain features on the Udyamonline.org</p></li>

                        <li><p>Beside this, the User is aware that He/she might face other data collection devices like cookies on certain pages which are installed by third parties or affiliates on the Udyamonline.org. The User knows and accepts that the Udyamonline.org/Company is not responsible as they do not control the use of such cookies/other devices by third parties and the User assumes any and all risks in this regard.</p></li>

                    </ol>

                    <h2 style="color: orange;">SHARING OF PERSONAL INFORMATION</h2>

                    <ol>

                        <li><p>The Udyamonline.org/Company can share the personal information of User to detect and prevent identity theft, fraud, and other illegal acts. This information can be shared with corporate entities and affiliates to help them; correlate related or multiple accounts to prevent abuse of the website services; and to facilitate joint or co-branded services, where such services are provided by more than one corporate entity. The User is aware of it.</p></li>

                        <li><p>The Udyamonline.org has right to disclose the personal information of User if required to do so by law or if the Udyamonline.org believes that such disclosure is reasonably necessary to respond to subpoenas, court orders, or other legal processes. This information can also be disclosed to law enforcement offices, third party rights owners, or other third parties if it believes that such disclosure is reasonably necessary to enforce the Terms or Policy; respond to claims that an advertisement, posting or another content violates the rights of a third party; or protect the rights, property or personal safety of its users, or the general public.</p></li>

                        <li><p>If a Company/Udyamonline.org is merging with other Company/website or in the event of reorganization, amalgamation or reconstructing of the Company’s business, the Company/Udyamonline.org can share the personal information of User if required. Such business entity or new entity will continue to be bound be the Terms and Policy, as may be amended from time to time.</p></li>

                    </ol>

                    <h2 style="color: orange;">THIRD PARTY ADVERTISEMENTS / PROMOTIONS</h2>

                    <p style="color: #5964C6;">The third-party advertising companies is used to serve ads to the users of the website. These companies may use the information related to the User’s visit to the Udyamonline.org and other websites in order to provide advertisements to the User. The Udyamonline.org may contain links to other websites also that may collect personal information about the User. The Company is not at all responsible for the privacy or the content of any of the linked websites. The User agrees that the risks associated will be borne entirely by the User.</p>

                    

                    <h2 style="color: orange;">USER’S CONSENT</h2>

                    <p style="color: #5964C6;">By using the Udyamonline.org and/ or by providing information to the Company through the website, the User consents to the collection and use of the information disclosed by the User on the website in accordance with this Policy.</p>

                    

                    <h2 style="color: orange;">DISPUTE RESOLUTION AND JURISDICTION</h2>

                    <p style="color: #5964C6;">Any dispute related to formation, interpretation and performance of this Policy will be solved through a two-step Alternate Dispute Resolution (“ADR”) mechanism. It is agreed by the Parties. It is further agreed to by the Parties that the contents of this Section shall survive even after the termination or expiry of the Policy and/or Terms.</p>



                    <ol>

                        <li><p>Mediation: If there would be any dispute between the parties, the Parties will attempt to resolve the dispute amongst themselves, with the mutual satisfaction of both Parties. If the Parties are unable to resolve the dispute between themselves within the period of thirty (30) days of one Party communicating the existence of a dispute to the other Party, the dispute will be resolved by arbitration, as detailed herein below;</p></li>

                        <li><p>Arbitration: If the Parties are unable to resolve the dispute between them through Mediation, then it will be transferred forward to the Arbitration by a sole arbitrator to be appointed by the Company. The order passed by the sole arbitrator will be valid and binding on both Parties. The costs of proceedings will be bear by the Parties, although the sole arbitrator may, in his/her sole discretion, direct either Party to bear the entire cost of the proceedings. The arbitration shall be conducted in English, and the seat of Arbitration shall be the city of Pune, India.</p></li>

                    </ol>

                    <p style="color: #5964C6;">The Terms, Policy and any other agreements between the Parties are governed by the laws, rules and regulations of India and it is agreed by the Parties. The Courts at Pune shall have exclusive jurisdiction over any disputes arising between the Parties.</p>

                </div>  

            </div>

        </div>

    </section>


@endsection
@section('js')
<script>

</script>
@endsection
