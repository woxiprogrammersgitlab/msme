@extends('layouts.master')
@section('content')
<section class="section" id="contact">
    <!-- Content -->
    <div class="container">
        @include ('partials.messages')
        <div class="row  align-items-center">
		<div class="col-md-3 col-lg-3">&nbsp;</div>

		<div class="col-md-6 col-lg-6 text-center text-lg-left" style="color:#000">
			<p>Hello <strong>{{$customer_name}}</strong>,</p>
			<p>We have received your payment of <strong>INR {{env('EXPERT_ADVICE_AMOUNT')}}</strong> & your request is confirmed. </p>
			<div style="border : 1px solid #888; padding:20px; background-color:#f2f2f2;text-align:center;">
				<p style="font-size: 20px;color:#5964c6;">Request Number : <strong>{{$orderId}}</strong></p>	
				<input type="hidden" id="orderId" name="orderId" value="{{$orderId}}">
			</div>
			<br/>
			<p style="font-weight:bolder;">Sit relax, our expert will call you during your preferred selected time slot : <strong>{{$timeslot}}</strong></p>
	        </div>
		<div class="col-md-3 col-lg-3">&nbsp;</div>

        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>
@endsection
@section('js')

<!-- Event snippet for Example conversion page -->
<script>
    var orderID = document.getElementById("orderId").value;
    gtag('event', 'conversion', {'send_to': 'AW-611021151/tUmJCLjdhNoBEN_iraMC',
    'value': {{env("EXPERT_ADVICE_AMOUNT")}},
    'currency': 'INR',
    'transaction_id': '{{$orderId}}',
    });
</script>

<script>
$(window).load(function() {
    $(".razorpay-payment-button").addClass("blue");
});

$( document ).ready(function() {
    $(".razorpay-payment-button").addClass("blue");
});
$("body").on("load", function() {
    $(".razorpay-payment-button").addClass("blue");
});
</script>
@endsection
