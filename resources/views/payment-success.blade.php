@extends('layouts.master')
@section('content')
<section class="section" id="contact">
    <!-- Content -->
    <div class="container">
        @include ('partials.messages')
        <div class="row  align-items-center">
		<div class="col-md-3 col-lg-3">&nbsp;</div>

		<div class="col-md-6 col-lg-6 text-center text-lg-left" style="color:#000">
			<p>Hello <strong>{{$customer_name}}</strong>,</p>
			<p>We have received your payment of <strong>INR {{env('REGISTRATION_AMOUNT')}}</strong> & your application is confirmed for processing. </p>
			<div style="border : 1px solid #888; padding:20px; background-color:#f2f2f2;text-align:center;">
				<p style="font-size: 20px;color:#5964c6;">Application Number : <strong>{{$orderId}}</strong></p>	
				<input type="hidden" id="orderId" name="orderId" value="{{$orderId}}">
				<a target="_blank" href="{{$applicationTrackUrl}}">
        	                	<button class="btn btn-primary" style="font-size:24px;color : #fff;background-color : #5964c6;border : 1px solid #5964c6;">Track Application</button>
	                	</a>

	
<!--                <div>Request Submmited Successfully.</div>
                <div>(Your request has been received.)</div>
                <h1>Token Number: {{$orderId}}</h1>
		<div>
			<a target="_blank" href="{{$applicationTrackUrl}}">
			<button class="btn btn-primary btn-circled">Track Request</button>
		</a>
		</div>
                <div>Please note that this Token number to check your application status.</div>
		<div>Please use this token number to know the status of your application.</div>
-->
			</div>
			<br/>
			<p style="font-weight:bolder;">Please note this Application Number can be used to track/enquire about your Application Status</p>
	        </div>
		<div class="col-md-3 col-lg-3">&nbsp;</div>

        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>
@endsection
@section('js')

<!-- Event snippet for Example conversion page -->
     <script>
     var orderID = document.getElementById("orderId").value;
     gtag('event', 'conversion', {'send_to': 'AW-611021151/tUmJCLjdhNoBEN_iraMC',
      'value': {{env("REGISTRATION_AMOUNT")}},
      'currency': 'INR',
      'transaction_id': '{{$orderId}}',
     });
    </script>

<script>
$(window).load(function() {
    $(".razorpay-payment-button").addClass("blue");
});

$( document ).ready(function() {
    $(".razorpay-payment-button").addClass("blue");
});
$("body").on("load", function() {
    $(".razorpay-payment-button").addClass("blue");
});
</script>
@endsection
