@extends('layouts.master')
@section('content')


    <section class="section" id="contact">

        <div class="container">

            <div class="row">

                <div class="col-lg-12" style="padding: 20px">

                    <h2 style="color: orange;">Refund Policy</h2>

                    <p style="color: #5964C6;">Udyamonline.org believes in providing services to all clients. We will only accept refunds requests if the service is not provided. Once submission of documents to Government authorities for various services offered is done, no applicant is eligible for any refund. We reserves the right to determine a fair value of the product on return and the same shall be binding on both parties.</p>

                    <p style="color: #5964C6;">The refund process will only be initiated once we get the confirmation of the services not provided. Refunds may take 7-10 working days to credit into applicant’s accounts. It may take few more days, if refunds are to be paid by cheque/NEFT. For any services not provided correctly, please send us a mail at support@Udyamonline.org. All cancellation requests has to be placed within 3 days from the date of order for further processing. If cancellation request is placed after 3 days from date of applying, professional fee is non-refundable. Once application is put into processing with the concerned government authorities, cancellations are non-refundable. Order is said to be fulfilled when payment made by applicant is successful and applicant got the pre filled computer generated application form. We do not entertain return.</p>



                </div>

            </div>

        </div>

    </section>

@endsection
@section('js')
<script>

</script>
@endsection
