@if (count($messages))
<div class="row">
  <div class="col-md-12">
  @foreach ($messages as $message)
    <div class="alert alert-{{ $message['level'] }} fade in alert-dismissible show" style="margin-top: 18px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true" style="font-size:20px">×</span>
        </button>
        <strong>{!! $message['message'] !!}</strong>
    </div>
  @endforeach
  </div>
</div>
@endif
