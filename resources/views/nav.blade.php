<!-- NAVBAR
================================================= -->
<div class="main-navigation" id="mainmenu-area">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary main-nav navbar-togglable rounded-radius">
            <a class="navbar-brand d-lg-none d-block" href="/">
		    <h2 style="color:#fff;font-size:20px;border-bottom: 1px solid #fff;">UDYAM<span style="color:#fff"> REGISTRATION</span></h2>
	            <h3 style="color: orange;font-size:15px;">MSME ONLINE CONSULTANCY</h3>
    	    </a>
            <!-- Toggler -->
            <button  class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span>
            </button>

            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <!-- Links -->
		<ul class="navbar-nav ">
		    <li class="nav-item ">
                        <a href="/home" class="nav-link js-scroll-trigger">
                            HOME
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="/udyam-registration" class="nav-link js-scroll-trigger">
                            UDYAM REGISTRATION
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="/msme-to-udyam-registration" class="nav-link js-scroll-trigger">
                            MSME/Udyog AADHAR To udyam
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="/expert-advice-phone" class="nav-link js-scroll-trigger">
                            Expert Advice
                            <img src="/images/new.gif" width="30px" height="20px"  style="margin-top:0px;" alt="new service logo" style="margin-top: -10px;">
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a href="/enquire-now" class="nav-link js-scroll-trigger">
                            Enquire Now
                            <img src="/images/new.gif" width="30px" height="20px"  style="margin-top:0px;" alt="new service logo" style="margin-top: -10px;">
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarWelcome" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            More
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarWelcome">
                                <a class="dropdown-item " href="/udyam-registration-procedure">
                                PROCEDURE
                            </a>
                            <a class="dropdown-item " href="/udyam-registration-sample-certificate">
                                SAMPLE CERTIFICATE
                            </a>
                            <a class="dropdown-item " href="/track-application">
                                TRACK Application
                            </a>
                            <a class="dropdown-item " href="/udyam-registration-faqs">
                                FAQ
                            </a>
                            <a class="dropdown-item " href="/udyam-registration-benefits">
                                BENEFITS
                            </a>
                            <a class="dropdown-item " href="/contact-us">
                                CONTACT US
                            </a>
                            <a class="dropdown-item " href="/about-us">
                                ABOUT US
                            </a>
                        </div>
                    </li>
                </ul>

                <!--<ul class="ml-lg-auto list-unstyled m-0">
                    <li><a href="#" class="btn btn-white btn-circled">Get a quote</a></li>
                </ul>-->
            </div> <!-- / .navbar-collapse -->
        </nav>
    </div> <!-- / .container -->
</div>
