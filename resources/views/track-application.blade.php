@extends('layouts.master')
@section('content')
@if(!request()->has('application_number'))
<section class="section" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12" style="border: 1px solid #5964C6;">
                <!-- Contacts Form -->
                <form id="track-form" class="contact_form" action="/track-application-submit" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-12 mb-12" style="background-color: #5964C6;text-align:center;color:#fff; font-size:20px;">
                            <p style="font-weight: bold;">Track Your Udyam Application Here</p>
                        </div>
                    </div>
                    <br/>
                    <div class="row">

                        <!-- Input -->
                        <div class="col-sm-6 mb-6">
                            <div class="form-group">
                                <label class="h6 small d-block text-uppercase">
                                    1. MOBILE NO / मोबाइल नंबर
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="input-group ">
                                    <input class="form-control {{ $errors->has('mobile') ? 'is-invalid' : ''}}" name="mobile" id="mobile" required="" placeholder="e.g 8888888888" type="text">
                                    @if($errors->has('mobile'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                        <!-- Input -->
                        <div class="col-sm-6 mb-6">
                            <div class="form-group">
                                <label class="h6 small d-block text-uppercase">
                                    2. APPLICATION NO / आदेश संख्या
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="input-group ">
                                    <input class="form-control {{ $errors->has('order_number') ? 'is-invalid' : ''}}" name="order_number" id="order_number" required="" placeholder="e.g UDM200726791" type="text">
                                    @if($errors->has('order_number'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('order_number') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->


			<!-- Input -->
                        <div class="col-sm-6 col-lg-6 mb-12">

				<div class="form-group">
                                <p style="color:#000;font-weight:bold;">SECURITY CHECK <span class="text-danger">*</span></p>
                                <fieldset style="border: 1px solid #5964c6;padding:10px;">
                                <p style="color:#000;">Please solve the following</p>
                                <div class="input-group">
        <label for="mathgroup" style="font-size:28px !important;color:black;">{{ app('mathcaptcha')->label() }} = </label> &nbsp;&nbsp;
                                    {!! app('mathcaptcha')->input(['class' => 'form-control', 'id' => 'mathgroup', 'required' => 'required']) !!}
                                    @if ($errors->has('mathcaptcha'))
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('mathcaptcha') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                </fieldset>
                            </div>
			</div>
                        <!-- End Input -->

                    <div class="col-sm-12 mb-12">
                        <div class="form-group">
                            <input name="submit" type="submit" class="btn btn-primary" value="Submit" style="font-size:16px;">
                        </div>
                    </div>
                </form>
                <!-- End Contacts Form -->
            </div>
        </div>
    </div>
</section>
@else
<section class="section" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @include ('partials.messages')
                <!-- Contacts Form -->
                @if(isset($data) && !is_null($data))
                <form class="contact_form" action="mail.php">
                    <div class="row">
                        <div class="col-sm-12 mb-12" style="text-align:center;color:#5964C6; font-size:20px;">
                            <p style="font-weight: bold;">Your Udyam Application Status /
                                आपका उदयम आवेदन की स्थिति</p>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <!-- Input -->
                        <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                <table class="form-group" width="100%" style="text-align: center;overflow-x:auto;" border="1">
                                    <tr style="background-color: #f2f2f2;font-weight:bold">
                                        <td>APPLICATION NO</td>
                                        <td>NAME OF ENTREPRENEUR/ APPLICANT</td>
                                        <td>Date of Submission</td>
                                        <td>Payment Status</td>
                                    </tr>
                                    <tr>
                                        <td>{{$data->application_number}}</td>
                                        <td>{{$data->entrepreneur_name}}</td>
                                        <td>{{$data->created_at->format('Y-m-d')}}</td>
                                        @if($data->status)
                                        <td>Completed</td>
                                        @else
                                        <td>In Progress</td>
                                        @endif
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- End Input -->
                    </div>
                </form>
                @endif
                <!-- End Contacts Form -->
            </div>
        </div>
    </div>
</section>
@endif
@endsection
@section('js')
<script>

</script>
@endsection
