@extends('layouts.master')
@section('content')
<section class="section" id="contact">
    <div class="container">
        @include ('partials.messages')
        <div class="row mb-4">
            <div class="col-md-8 col-lg-6">
                <!-- Heading -->
                <h2 class="section-title mb-2 ">
                    Get a Call <span class="font-weight-normal">back</span>
                </h2>

                <!-- Subheading -->
                <p class="mb-5 ">
                    If you have any question, Please leave your number we will call you back or Catch us on support@udyamonline.org
                    OR WhatsApp On +91 9665256547
                </p>

            </div>
        </div> <!-- / .row -->

        <div class="row">
            <div class="col-lg-12">
               <!-- form message -->
                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-success contact__msg" style="display: none" role="alert">
                            Your message was sent successfully.
                        </div>
                    </div>
                </div>
                <!-- end message -->
                <!-- Contacts Form -->
                <form id="contact_form" class="contact_form" action="/contact-us-submit" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <!-- Input -->
                        <div class="col-sm-6 mb-6">
                            <div class="form-group">
                                <label class="h6 small d-block text-uppercase">
                                    Your name
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="input-group">
                                    <input value="{{old('name')}}" class="form-control  {{ $errors->has('name') ? 'is-invalid' : ''}}" name="name" id="name" required="" placeholder="Please enter your name here" type="text">
                                    @if($errors->has('name'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                        <!-- Input -->
                        <div class="col-sm-6 mb-6">
                            <div class="form-group">
                                <label class="h6 small d-block text-uppercase">
                                    Your email address
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="input-group ">
                                    <input value="{{old('email')}}" class="form-control  {{ $errors->has('email') ? 'is-invalid' : ''}}" name="email" id="email" required="" placeholder="Please enter your email here" type="email">
                                    @if($errors->has('email'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->


                        <div class="w-100"></div>

                        <!-- Input -->
                        <div class="col-sm-6 mb-6">
                            <div class="form-group">
                                <label class="h6 small d-block text-uppercase">
                                    Your Phone Number
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="input-group ">
                                    <input value="{{old('mobile')}}" maxlength="10" class="form-control  {{ $errors->has('mobile') ? 'is-invalid' : ''}}" id="mobile" name="mobile" required="" placeholder="Please enter your mobile number here" type="text">
                                    @if($errors->has('mobile'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                <div class="col-sm-6 mb-6">
                    <!-- Input -->
                    <div class="form-group mb-6">
                        <label class="h6 small d-block text-uppercase">
                            How can we help you?
                        </label>

                        <div class="input-group">
                            <textarea class="form-control  {{ $errors->has('message') ? 'is-invalid' : ''}}" rows="11111111111" name="message" id="message" required="" placeholder="Please write your message here">{{old('message')}}</textarea>
                            @if($errors->has('message'))
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('message') }}</strong>
                            </div>
                            @endif
                        </div>
                    </div>
                    <!-- End Input -->
                    </div>




			</div>




			<div class="row">
		<!-- Input -->
                        <div class="col-sm-6 mb-6">
			
				<div class="form-group">
                                <p style="color:#000;font-weight:bold;">SECURITY CHECK <span class="text-danger">*</span></p>
                                <fieldset style="border: 1px solid #c2c2c2;padding:10px;">
                                <p style="color:#000;">Please solve the following</p>
                                <div class="input-group">
        <label for="mathgroup" style="font-size:28px !important;color:black;">{{ app('mathcaptcha')->label() }} = </label> &nbsp;&nbsp;
                                    {!! app('mathcaptcha')->input(['class' => 'form-control', 'id' => 'mathgroup', 'required' => 'required']) !!}
                                    @if ($errors->has('mathcaptcha'))
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('mathcaptcha') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                </fieldset>
                            </div>
			</div>
                        <!-- End Input -->

		</div>




                    <div class="">
                       <input name="submit" type="submit" class="btn btn-primary" value="Send Message" style="font-size:16px;">

                        <p class="small pt-3">We'll get back to you in 24hrs.</p>
                    </div>
                </form>
                <!-- End Contacts Form -->
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
<script>

</script>
@endsection
