@extends('layouts.master')
@section('content')

<!-- Modal -->
<div id="successModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
</div>
<style>
#rzp-button1 { 
font-size : 24px;
width : 100%;
color : #fff;
background-color : #5964c6;
border : 1px solid #5964c6;
}

#banner .overlay {
    opacity: 0.7;
}

.overlay {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
}

#cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgb(89, 100, 198, 0.4);
    z-index:9999;
    display:none;
}

@-webkit-keyframes spin {
	from {-webkit-transform:rotate(0deg);}
	to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
	from {transform:rotate(0deg);}
	to {transform:rotate(360deg);}
}

#cover-spin::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}

</style>

<div id="cover-spin"></div>


<section class="banner-area py-5" id="banner" style="background: url('/images/bg/banner-image-3.jpg') no-repeat;">
       <div class="overlay"></div>
        <!-- Content -->
        <div class="container">
            <div class="row ">
            <div class="col-md-8 col-lg-8" style="margin-top:-100px">
                   <div class="banner-content text-lg-left">
                        <h1 style="color:#F7A630;font-size:70px;font-family: 'Times New Roman', Times, serif;line-height: 1em;">
                            Talk To Our Expert
                        </h1>
                        <br/>
                        <br/>
                        <!-- Heading -->
                        <p style="color:#fff;font-size:36px;">Having Doubts related to MSME / Udyam Registration?</p>
                        <!-- Subheading -->
                        <ul style="color:#e2e2e2;font-size:30px;">
                            <li>Submit your <span style="color:#fff;font-weight:bold;">Query</span></li>
                            <li>Select a Preferable <span style="color:#fff;font-weight:bold;">Time Slot</span></li>
                            <li>Book an <span style="color:#fff;font-weight:bold;">Appointment</span> & get your <span style="color:#fff;font-weight:bold;">Doubts Cleared</span></li>
                        </ul>
                        <p style="color:#fff;font-size:36px;">
                            <i>&nbsp;&nbsp;&nbsp;&nbsp;Just @  <span style="color:#F7A630;font-weight:bold;">Rs. {{round(env('EXPERT_ADVICE_AMOUNT'), 0)}}</span></i>
                        </p>
                    </div>
                </div>

            <div class="col-lg-4 ">
                    <div class="banner-contact-form bg-white" style="padding : 20px 10px;">
                    <form id="expert_advice" class="contact_form" action="/expert-advice-form-submit" method="POST">
                        {{ csrf_field() }}
                        <!-- Input -->
                        <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                <!--<label class="h6 small d-block text-uppercase">
                                    Your name
                                    <span class="text-danger">*</span>
                                </label>-->

                                <div class="input-group ">
                                    <input class="form-control {{ $errors->has('customer_name') ? 'is-invalid' : ''}}" name="customer_name" id="customer_name" value="{{old('customer_name')}}" required="required" placeholder="Your Name" type="text">
                                    @if($errors->has('customer_name'))
                                    <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('customer_name') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                        <!-- Input -->
                        <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                <!--<label class="h6 small d-block text-uppercase">
                                    Mobile No +91-
                                    <span class="text-danger">*</span>
                                </label>-->

                                <div class="input-group ">
                                    <input class="form-control {{ $errors->has('mobile_number') ? 'is-invalid' : ''}}" name="mobile_number" id="mobile_number" value="{{old('mobile_number')}}" maxlength="10" required="" placeholder="Mobile No +91-" type="text">
                                    @if($errors->has('mobile_number'))
                                    <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                        <!-- Input -->
                        <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                <!--<label class="h6 small d-block text-uppercase">
                                    Email ID
                                    <span class="text-danger">*</span>
                                </label>-->

                                <div class="input-group ">
                                    <input class="form-control {{ $errors->has('customer_email') ? 'is-invalid' : ''}}" name="customer_email" id="customer_email" value="{{old('customer_email')}}" required="required" placeholder="Your Email ID" type="email">
                                    @if($errors->has('customer_email'))
                                    <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('customer_email') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                        <!-- Input -->
                         <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                <!--<label class="h6 small d-block text-uppercase">
                                    Issues Related to
                                    <span class="text-danger">*</span>
                                </label>-->

                                <div class="input-group ">
                                    <select required="required" class="form-control {{ $errors->has('issue_related_to_id') ? 'is-invalid' : ''}}" name="issue_related_to_id" id="issue_related_to_id" value="{{old('issue_related_to_id')}}">
                                        <option value="">Select Issues Related to</option>
                                        @foreach ($issueRelatedTo as $issue)
                                            @if ($issue->id != 1)
                                                <option value="{{$issue->id}}">{{$issue->name}}</option>
                                            @endif
                                        @endforeach
                                        <option value="1">Others? Please specify</option>
                                    </select>
                                    @if($errors->has('issue_related_to_id'))
                                    <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('issue_related_to_id') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                        <!-- Input -->
                        <div class="col-sm-12 mb-12" id="other_issue_div">
                            <div class="form-group">
                                <div class="input-group ">
                                    <input class="form-control {{ $errors->has('other_issue') ? 'is-invalid' : ''}}" name="other_issue" id="other_issue" value="{{old('other_issue')}}"  placeholder="Please specify your issue here" type="text">
                                    @if($errors->has('other_issue'))
                                    <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('other_issue') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                    <!-- Input -->
                    <div class="col-sm-12 mb-12">
                        <div class="form-group">
                        <!--<label class="h6 small d-block text-uppercase">
                            Your Message
                            <span class="text-danger">*</span>
                        </label>-->

                        <div class="input-group">
                            <textarea class="form-control  {{ $errors->has('customer_message') ? 'is-invalid' : ''}}" rows="3" cols="4" name="customer_message" id="customer_message" required="required" placeholder="Please write your message here">{{old('customer_message')}}</textarea>
                            @if($errors->has('customer_message'))
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('customer_message') }}</strong>
                            </div>
                            @endif
                        </div>
                        </div>
                    </div>
                    <!-- End Input -->

                    <!-- Input -->
                    <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                <!--<label class="h6 small d-block text-uppercase">
                                    Your preferable time slot
                                    <span class="text-danger">*</span>
                                </label>-->

                                <div class="input-group ">
                                    <select required="required" class="form-control {{ $errors->has('preferable_slot') ? 'is-invalid' : ''}}" name="preferable_slot" id="preferable_slot" value="{{old('preferable_slot')}}">
                                        <option value="">Select preferable time slot</option>
                                        @foreach ($timeSlot as $key=>$value)
                                            <option value="{{$value}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('preferable_slot'))
                                    <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('preferable_slot') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                    <!-- Input -->
                    <div class="col-sm-12 mb-12">
			
                    <div class="form-group">
                            <!--<p style="color:#000;font-weight:bold;">SECURITY CHECK <span class="text-danger">*</span></p>-->
                            <fieldset style="border: 1px solid #c2c2c2;padding:10px;">
                            <p style="color:#000;">Security Check</p>
                            <div class="input-group">
                                 <label for="mathgroup" style="font-size:28px !important;color:black;">{{ app('mathcaptcha')->label() }} = </label> &nbsp;&nbsp;
                                {!! app('mathcaptcha')->input(['class' => 'form-control', 'id' => 'mathgroup', 'required' => 'required']) !!}
                                @if ($errors->has('mathcaptcha'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mathcaptcha') }}</strong>
                                    </div>
                                @endif
                            </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- End Input -->

                     <!-- Input -->
                     <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                   <div class="input-group">
                                    <input required="required" class="{{ $errors->has('t_and_c') ? 'is-invalid' : ''}}" type="checkbox" style="margin: 7px;" id="t_and_c" name="t_and_c" value="true">&nbsp;<span style="color: darkorange;">I agree to the <a href="/udyam-registration-terms-and-condition" target="_blank">T&C and disclaimer.</a></span></input>
                                    @if($errors->has('t_and_c'))
                                    <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('t_and_c') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->
                        
                        <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                <input name="submit" type="submit" class="btn btn-primary" id="rzp-button1" value="Register Now" style="font-size:20px;width:100%">
                                <!--<p class="small pt-3">We'll get back to you in 24hrs.</p>-->
                            </div>
                        </div>
                            
                        </form>
                    </div>
                </div>
  
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </section>

@endsection
@section('js')

<script>

    $( document ).ready(function() {
        $('#other_issue_div').hide();
        $("#issue_related_to_id").on('change', function(){
            if($("#issue_related_to_id").val() == 1) {
                $('#other_issue_div').show();
            } else {
                $('#other_issue_div').hide();
            }
        });
        //$("#modal-data-id").html(resp);
        //$("#successModal").modal('show');

    });

</script>
@endsection

