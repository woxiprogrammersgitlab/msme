@extends('layouts.master')
@section('content')

<section class="section" id="contact">

        <div class="container">

            <div class="row">

                <div class="col-lg-12" style="padding: 20px">

                    <h2 style="color: orange;">DISCLAIMER POLICY</h2>

                    <p style="color: #5964C6;">This disclaimer is applicable to Udyamonline.org. By using or accessing this website you agree with the terms and conditions in this disclaimer. This is just a website that assists business houses to make application for registration or update Udyog Aadhar/ Udyam Registration. This is not a Government Website. We are Private consultants managing this website and providing Financial and Legal assistance for Customer Benefits. Although the greatest possible care has been exercised to ensure the correctness and completeness of the information provided on this website, we do not accept any liability for it.</p>



                    <h2 style="color: orange;">Terms and Conditions</h2>

                    <p style="color: #5964C6;">User can access “Udyamonline.org” (the “site”) subject to the terms and conditions of use, as changed / updated from time to time entirely at the discretion of Udyamonline.org as set out below. The term of this Agreement shall continue and would be in force till the time you access this site & its services and you agree that you are bound by the terms as mentioned herein. Nothing in these Terms of Service should be construed to confer any rights on you or any third party beneficiaries.</p>

                    <ol>

                        <li>

                            <strong>Eligibility and registration</strong>

                            <p>You must be 18 years or older to use the Udyamonline.org service. By registering for the Udyamonline.org service, you represent and warrant that you are 18 years or older and that you have the capacity to understand, agree to and comply with these Terms of Service.</p>

                            <p>Since Indian laws do not permit to us to contract with a minor person (minor is a person who has not completed 18 years of age), we do not market our Web site to minor person under the age of 18. However, a minor can apply for PAN card under the supervision and guidance of guardian.</p>

                        </li>

                        <li>

                            <strong>Appropriate Individual Registration, access & conduct of user and content for personal use only</li></strong>

                            <p>Udyamonline.org service is made available for your personal, non-commercial use only. You cannot use Udyamonline.org service for any illegal or unauthorized purpose, including misusing permanent account number (PAN) of another person.</p>

                        </li>

                        <li>

                            <strong>Charges/Fee</strong>

                            <p>Udyamonline.org reserves the right to charge subscription and / or membership fees/ consultancy fee in respect of any part, aspect of this Site & Services and/or products upon reasonable prior notice. Such notice will be appearing on the site by displaying the new / revised charges for the services and/or products as offered from time to time with or without modifications & on such display it will be presumed to be read by all users whether registered or not & no further notice would be required. The same will apply for any service/s or products, which are currently being offered without any charges, whenever made chargeable.</p>

                        </li>

                        <li>

                            <strong>Our Rights </strong>

                            <p>We reserves its right to terminate your account without any prior notice for any violation of the Terms of Use</p>

                            <ul>

                                <li>

                                    <p>By submitting or furnishing any personal information, data or details on or through the Udyamonline.org services, you automatically grant to us non-exclusive, permission & irrevocable right to hold & store your, such personal information, data or details and related information. Our use of your personal information is governed by our Privacy Policy and we will never rent, sell or share your personal information with any third party for marketing purposes without your express permission.</p>

                                </li>

                                <li>

                                    <p>Only information you have given us (including email address) is stored by us. We do not supplement your information with information from other companies.</p>

                                </li>

                                <li>

                                    <p>If you do not want to receive promotional email from us in the future, please let us know by sending an email to us at the above address or writing to us at the above address and telling us that you do not want to receive e-mail from our company.</p>

                                </li>

                            </ul>

                        </li>

                        <li>

                            <strong>User Conduct</strong>

                            <p>You are solely responsible for your conduct and any data, text, information, graphics that you submit on to Udyamonline.org service. You cannot use this site for illegal or unauthorized purpose. Illegal or unauthorized uses include, but are not limited to:</p>

                            <ul>

                                <li><p>Modifying, adapting, translating, or reverse engineering any portion of the Udyamonline.org service.</p></li>

                                <li><p>Using any robot, spider, site search/retrieval application, or other device to retrieve or index any portion or the Udyamonline.org service.</p></li>

                                <li><p>Creating user accounts by automated means or under false or fraudulent pretenses.</p></li>

                                <li><p>Submitting data of any third party without such third party's prior written consent.</p></li>

                                <li><p>Transmitting any viruses, worms, defects, Trojan horses or other items of a destructive nature.</p></li>

                                <li><p>Creating or transmitting unwanted electronic communications such as "spam," or chain letters to or otherwise interfering with other user's enjoyment of the service by blocking.</p></li>

                                <li><p>Any other person sharing your User Name and Password</p></li>

                                <li><p>Any part of the Site being cached in proxy servers and accessed by person/s who have not registered with Udyamonline.org as users of the Site & Services</p></li>

                                <li><p>Submitting false or misleading information.</p></li>

                            </ul>

                        </li>

                        <li>

                            <strong>Legal Disclaimers</strong>

                            <p>Udyamonline.org has not reviewed any or all of the web sites linked to its Web Site and is not responsible for the content of any off-site pages or any other web sites linked to the Web Site. Please understand that any non Udyamonline.org web site is independent from Udyamonline.org and Udyamonline.org has no control over the content on that web site. In addition, a link to a non - Udyamonline.org web site does not mean that Udyamonline.org endorses or accepts any responsibility for the content, or the use, of such site. It is the user's responsibility to take precautions to ensure that whatever is selected is free of such items as viruses, worms, Trojan horses and other items of a destructive nature.</p>

                            <p>The information on the Web Site may contain technical inaccuracies or typographical errors. Any information on the Web Site including contents, terms and conditions may be changed or updated by Udyamonline.org without notice. User is bound by such revisions and should therefore periodically visit website to review the then current contents, information, terms and conditions etc.</p>

                        </li>

                        <li>

                            <strong>Limitation on Warranties</strong>

                            <p>All materials on the web site are provided on "As Is" basis. Udyamonline.org makes no representations or warranties, express or implied, including, but not limited to, warranties of merchantability, fitness for a particular purpose, title or non-infringement. As to documents and graphics published on the web site. Udyamonline.org makes no representation or warranty that the contents of such document or graphics are free from error or suitable for any purpose; nor that implementation of such contents will not infringe any third party patents, copyrights, trademarks or other rights.</p>

                            <p>Please note that some jurisdictions may not allow the exclusion of implied warranties, so some of the above exclusions may not apply to you.</p>

                        </li>

                        <li>

                            <strong>Limitation on Liability</strong>

                            <p>In no event shall Udyamonline.org, be liable to any party for any direct, indirect, special or consequential damages for any use of the web site, or on any other hyperlinked web site, including, without limitation, any lost profits, business interruption, loss of programs or other data on your information handling system or otherwise, even if Udyamonline.org is expressly advised of the possibility of such damages.</p>

                            <p>You hereby disclaim and waive any rights and claims you may have against Udyamonline.org with respect to third party products and services, to the maximum extent permitted by law.</p>

                        </li>

                        <li>

                            <strong>Indemnity</strong>

                            <p>You agree to defend, indemnify, and hold harmless Udyamonline.org and/or its associate entities, their officers, directors, employees and agents, from and against any claims, actions or demands, including without limitation reasonable legal and accounting fees, alleging or resulting from your use of the web site material or your breach of these Terms and Conditions of Web site use.</p>

                        </li>

                    </ol>

                    <p style="color: #5964C6;">I HAVE CAREFULLY READ AND UNDERSTOOD THE TERMS AND CONDITIONS MENTIONED HEREINABOVE AND HEREBY CONFIRM MY ACCEPTANCE TO THE SAME. I SHALL ADHERE TO ALL THE TERMS AND CONDITIONS MENTIONED IN THIS TERMS OF SERVICE.</p>

                    <p style="color: #5964C6;">The services shall be subject to the terms of service, privacy policy and disclaimers given at www.udyamonline.org</p>

                </div>

            </div>

        </div>

    </section>

@endsection
@section('js')
<script>

</script>
@endsection
