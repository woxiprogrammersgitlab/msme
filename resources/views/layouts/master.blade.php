<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">
@if(isset($seoData))
<meta name="description" content="{{$seoData['description']}}">
<meta name="keywords" content="{{$seoData['keywords']}}">
<title>{{$seoData['title']}}</title>
@else
<meta name="description" content="Udyam Registration">
<meta name="keywords" content="Udyam, udyam Registration, msme registration">
<title>Udyam Registration &amp; MSME ONLINE CONSULTANCY</title>
@endif


<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">

<!-- bootstrap.min css -->
<link rel="stylesheet" href="/plugins/bootstrap/css/bootstrap.min.css">
<!-- Animate Css -->
<link rel="stylesheet" href="/plugins/animate-css/animate.css">
<!-- Icon Font css -->
<link rel="stylesheet" href="/plugins/fontawesome/css/all.css">
<link rel="stylesheet" href="/plugins/fonts/Pe-icon-7-stroke.css">
<!-- Themify icon Css -->
<link rel="stylesheet" href="/plugins/themify/css/themify-icons.css">
<!-- Slick Carousel CSS -->
<link rel="stylesheet" href="/plugins/slick-carousel/slick/slick.css">
<link rel="stylesheet" href="/plugins/slick-carousel/slick/slick-theme.css">

<!-- Main Stylesheet -->
<link rel="stylesheet" href="/css/style.css">

@if (env('APP_ENV') == 'production')

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5ZLWGXQ');</script>
<!-- End Google Tag Manager -->

<script data-ad-client="ca-pub-8364427342868441" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

@endif


</head>


<body id="top-header">

@if (env('APP_ENV') == 'production')

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZLWGXQ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

@endif


    {{-- @yield('navBar') --}}
    <!-- LOADER TEMPLATE -->
<div id="page-loader">
    <div class="loader-icon fa fa-spin colored-border"></div>
    </div>
    <!-- /LOADER TEMPLATE -->
    <div class="logo-bar d-none d-md-block d-lg-block bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="logo d-none d-lg-block">
                        <!-- Brand -->
                        <a class="navbar-brand js-scroll-trigger" href="/">
                            <!--<h2 style="border-bottom: 1px solid #5964C6;margin-top:-12px;">UDYAM<span style="color:#5964C6"> REGISTRATION</span></h2>
			    <h3 style="color: orange;font-size:20px;">MSME ONLINE CONSULTANCY</h3>-->
			    <img src="/images/udyam-registration-logo.jpg" alt="udyam registration logo" style="margin-top: -10px;">
                        </a>
                    </div>
                </div>

                <div class="col-lg-8 justify-content-end ml-lg-auto d-flex col-12 col-md-12 justify-content-md-center">
                    <div class="top-info-block d-inline-flex">
                        <div class="icon-block">
                            <i class="ti-mobile"></i>
                        </div>
                        <div class="info-block">
                            <h5 class="font-weight-500">9665256547</h5>
                            <p>Whatsapp Us</p>
                        </div>
                    </div>

                    <div class="top-info-block d-inline-flex">
                        <div class="icon-block">
                            <i class="ti-email"></i>
                        </div>
                        <div class="info-block">
                            <h5 class="font-weight-500">support@udyamonline.org</h5>
                            <p>Email Us</p>
                        </div>
                    </div>
                    <div class="top-info-block d-inline-flex">
                        <img src="/images/MSME.png" width="200px" height="70px">
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('nav')
    @yield('content')
    @include('footer')
<!--
Essential Scripts
=====================================-->


<!-- Main jQuery -->
<script src="/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 3.1 -->
<script src="/plugins/bootstrap/js/popper.min.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Slick Slider -->
<script src="/plugins/slick-carousel/slick/slick.min.js"></script>
<script src="/plugins/jquery/jquery.easing.1.3.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/js/additional-methods.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="/js/validation-methods.js"></script>
<script src="/js/expert-advice-validation.js"></script>
<script src="/js/enquiry-now-validation.js"></script>

<!-- Map Js -->
<!--<script src="plugins/google-map/gmap3.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwIQh7LGryQdDDi-A603lR8NqiF3R_ycA"></script>-->

<!--<script src="/plugins/form/contact.js"></script>-->
<script src="/js/theme.js"></script>
<script src="{{ URL::asset('js/theme.js') }}"></script>


@yield('js')

</body>
</html>
