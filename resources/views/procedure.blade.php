@extends('layouts.master')
@section('content')



<section class="section bg-grey" id="feature" style="padding: 3.5rem 0;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 text-center">
                    <div class="section-heading">
                        <!-- Heading -->
                        <h2>
                            Procedure to avail Udyam Certificate
                        </h2>
                    </div>
                </div>
            </div> <!-- / .row -->
            <div class="row justy-content-center">
                <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="text-center feature-block">
                        <div class="img-icon-block mb-4">
                            <i class="ti-thumb-up"></i>
                        </div>
                        <h4 class="mb-2">Fill Up Application Form</h4>
                        <!--<p>Our team are experts in matching you with the right provider.</p>-->
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="text-center feature-block">
                        <div class="img-icon-block mb-4">
                            <i class="ti-wallet"></i>
                        </div>
                        <h4 class="mb-2">Make Online Payment</h4>
                        <!--<p>We've been awarded for our high rate of customer satisfaction.</p>-->
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="text-center feature-block">
                        <div class="img-icon-block mb-4">
                            <i class="ti-dashboard"></i>
                        </div>
                        <h4 class="mb-2">Executive Will Process Application</h4>
                        <!--<p>We only compare market leaders with a reputation for service quality.</p>-->
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="text-center feature-block">
                        <div class="img-icon-block mb-4">
                            <i class="ti-email"></i>
                        </div>
                        <h4 class="mb-2">Receive Certificate On Mail</h4>
                        <!--<p>We only compare market leaders with a reputation for service quality.</p>-->
                    </div>
                </div>
            </div>
        </div> <!-- / .container -->
    </section>

@endsection
@section('js')
<script>

</script>
@endsection

