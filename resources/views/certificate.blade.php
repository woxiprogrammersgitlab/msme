@extends('layouts.master')
@section('content')
<section class="banner-area py-7" style="padding: 2.5rem 0 !important;">
    <!-- Content -->
    <div class="container">
        <div class="row  align-items-center">
	    <div class="col-md-12 col-lg-12 text-center">
		<br/>
	       <p style="text-align:left">Post a fix processing period as communicated by our officials, an Udyam Registration Certificate is received by the applicant once the entire online registration process is complete. This Certificate is valid for lifetime. </p>
		<p style="text-align:left">A reference Udyam registration Certificate is as shown below :</p>
               <img src="images/sample-msme-certificate.png" width="100%">
            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>

@endsection
@section('js')
<script>

</script>
@endsection

