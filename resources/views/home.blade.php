@extends('layouts.master')
@section('content')

 <!-- HERO

    ================================================== -->

    <section class="banner-area py-5" id="banner">

       <div class="overlay"></div>

        <!-- Content -->

        <div class="container">

            <div class="row  align-items-center justify-content-center">

                <div class="col-md-12 col-lg-12">

                   <div class="banner-content text-center text-lg-left" style="padding-right:0px !important">

                        <!-- Heading -->

                        <h1 class="display-4 mb-4 ">

                            India's Most Trusted Online Portal For Registration under UDYAM / MSME / SSI / UDYOG AADHAR

			</h1>
		


                        <!-- Subheading -->

                        <p class="lead mb-5">

			<marquee style="font-size: 16px;opacity:0.7;color:black;" bgcolor="orange" width="100%">This website is owned and managed by private consultancy organization and is not a part of Ministry of Micro, Small and Medium Enterprises.</marquee>

                        </p>



                        <!-- Button -->

                        <p class="mb-0">

                            <a href="/udyam-registration" target="_blank" class="btn btn-primary" style="font-size:16px;text-transform: capitalize;">

				Not Yet Registered Under UDYAM/MSME/SSI/UDYOG AADHAR ? <span style="color:orange">Click here</span>
                            </a>

			</p>

			<p class="mb-0">

                            <a href="/msme-to-udyam-registration" target="_blank" class="btn btn-primary" style="font-size:16px;text-transform: capitalize;">

				Already Registered Under UDYAM/MSME/SSI/UDYOG AADHAR ? <span style="color:orange">Click here</span>
                            </a>

			</p>
			<h4 style="color:#fff">
				Trusted By 5000+ MSME In India
                        </h4>



                    </div>

                </div>

                

<!--                <div class="col-lg-4 ">

                    <div class="banner-contact-form bg-white">

                        <form action="#" >

                            <div class="form-group">

                                <input type="text" name="name" id="name" class="form-control" placeholder="your Name">

                            </div>

                            <div class="form-group">

                                <input type="text" name="email" id="email" class="form-control" placeholder="your Email">

                            </div>

                            <div class="form-group">

                                <input type="text" class="form-control" name="subject" id="subject" placeholder="your Subject">

                            </div>

                            <div class="form-group">

                                <textarea name="message" id="message" cols="30" rows="4" class="form-control" placeholder="Your message"></textarea>

                            </div>

                            <a href="#" class="btn btn-dark btn-block btn-circled">Submit a query</a>

                        </form>

                    </div>

                </div>-->

            </div> <!-- / .row -->

        </div> <!-- / .container -->

    </section>




 <section class="section" id="services-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 text-center">
                    <div class="section-heading">
                        <!-- Heading -->
                        <h2 class="section-title mb-2  text-white">
                            Benefits of Udyam/MSME/Udyog Aadhar Registration In India
                        </h2>

                        <!-- Subheading -->
                        <p class="mb-5 text-white" style="width: 100%;">
                            The Indian government has always been in favour of providing benefits to Micro, small and medium enterprises (MSMEs). There are many advantages of obtaining Udyam registration in India, which can be only availed if the business had registered itself as an MSME/SSI under MSME Act. The following are a few advantages or obtaining Udyam/SSI/MSME registration in India:
                        </p>
                    </div>
                </div>
            </div> <!-- / .row -->

            <div class="row">
                <div class="col-lg-4 col-sm-6 col-md-6 mb-30">
                    <div class="web-service-block">
                        <h3>Industrial Promotion Subsidy Eligibility</h3>
                        <p>Enterprises registered under MSME are also eligible for a subsidy for Industrial Promotion as suggested by the Government.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6 mb-30">
                    <div class="web-service-block">
                        <h3>Subsidy on Patent Registration</h3>
                        <p>A hefty 50% subsidy is given to the Enterprise that has the certificate of registration granted by MSME. This subsidy can be availed for patent registration by giving application to respective ministry.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6 mb-30">
                    <div class="web-service-block">
                        <h3>Overdraft Interest Rate Exemption</h3>
                        <p>Businesses or enterprises registered under MSME/Udyam can avail a benefit of 1% on the Over Draft as mentioned in a scheme that differs from bank to bank.</p>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6 col-md-6 ">
                    <div class="web-service-block">
                        <h3>Bank Loans (Collateral Free)</h3>
                        <p>The Government of India has made collateral-free credit available to all small and micro business sectors. This initiative guarantees funds to micro and small sector enterprises. Under this scheme, both the old as well as the new enterprises can claim the benefits. A trust named The Credit Guarantee Trust Fund Scheme was introduced by the GOI(Government Of India), SIDBI(Small Industries Development Bank Of India) and the Ministry of Micro, Small and Medium Enterprise to make sure this scheme is implemented (Credit Guarantee Scheme) for all Micro and Small Enterprise.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6 ">
                    <div class="web-service-block">
                        <h3>Fewer Electricity Bills</h3>
                        <p>This concession is available to all the Enterprises that have the MSME Registration Certificate by providing an application to the department of the electricity along with the certificate of registration by MSME.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6 ">
                    <div class="web-service-block">
                        <h3>ISO Certification Charges Reimbursement</h3>
                        <p>The registered MSME enterprises can claim the reimbursement of the expenses that were spent for the ISO certification.</p>
                    </div>
                </div>
                <div class="col-lg-12 col-sm-6 col-md-6" style="text-align:center;">
                    <div class="form-group">
                        <a href="/udyam-registration-benefits" class="btn btn-primary" style="font-size:16px">Read More</a>
                    </div>
                </div>
            </div>

        </div>
    </section>

	<section class="section bg-grey" id="feature" style="padding: 3.5rem 0;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 text-center">
                    <div class="section-heading">
                        <!-- Heading -->
                        <h2>
                            Procedure to avail Udyam Certificate
                        </h2>
                    </div>
                </div>
            </div> <!-- / .row -->
            <div class="row justy-content-center">
                <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="text-center feature-block">
                        <div class="img-icon-block mb-4">
                            <i class="ti-thumb-up"></i>
                        </div>
                        <h4 class="mb-2">Fill Up Udyam/MSME/Udyog Aadhar Application Form</h4>
                        <!--<p>Our team are experts in matching you with the right provider.</p>-->
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="text-center feature-block">
                        <div class="img-icon-block mb-4">
                            <i class="ti-wallet"></i>
                        </div>
                        <h4 class="mb-2">Make Online Payment</h4>
                        <!--<p>We've been awarded for our high rate of customer satisfaction.</p>-->
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="text-center feature-block">
                        <div class="img-icon-block mb-4">
                            <i class="ti-dashboard"></i>
                        </div>
                        <h4 class="mb-2">Executive Will Process Application</h4>
                        <!--<p>We only compare market leaders with a reputation for service quality.</p>-->
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6">
                    <div class="text-center feature-block">
                        <div class="img-icon-block mb-4">
                            <i class="ti-email"></i>
                        </div>
                        <h4 class="mb-2">Receive Certificate On Mail</h4>
                        <!--<p>We only compare market leaders with a reputation for service quality.</p>-->
                    </div>
                </div>
            </div>
        </div> <!-- / .container -->
    </section>



@endsection
@section('js')
<script>

</script>
@endsection

