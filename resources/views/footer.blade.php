<footer class="section " id="footer">
        <div class="overlay footer-overlay"></div>
        <!--Content -->
        <div class="container">
            <div class="row justify-content-start">
                <div class="col-lg-6 col-sm-12">
                    <div class="footer-widget">
                        <!-- Brand -->
                        <a href="/" class="footer-brand text-white">
                            <h2 style="font-size:30px;color:#fff;">UDYAM<span style="color:#5964C6"> REGISTRATION</span></h2>
                            <h3 style="color: orange;font-size:20px;">MSME ONLINE CONSULTANCY</h3>
                        </a>
                        <p style="text-align:justify;">Greetings from UDYAMONLINE.ORG. We are leading online Udyam Registration service provider. We have launched https://udyamonline.org/ with the aim of serving our clients with best and reliable online assistance for Udyam/MSME or Udyog Aadhaar registration in India. Our motive is to help the business establishments on the legal and regulatory requirements, and be a partner throughout the business lifecycle, offering support at every stage to ensure the business remains compliant and continually growing.</p>
                    </div>
                </div>

                <div class="col-lg-2 ml-lg-auto col-sm-12">
                    <div class="footer-widget">
                        <h3>Policy</h3>
                        <!-- Links -->
                        <ul class="footer-links ">
                            <li>
                                <a href="/udyam-registration-terms-and-condition">
                                    Terms and conditions
                                </a>
                            </li>
                            <li>
                                <a href="/udyam-registration-privacy-policy">
                                    Privacy policy
                                </a>
                            </li>
                            <li>
                                <a href="/udyam-registration-refund-policy">
                                    Refund Policy
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>


                <div class="col-lg-2 col-sm-6">
                    <div class="footer-widget">
                        <h3>More</h3>
                        <!-- Links -->
                        <ul class="footer-links ">
                            <li>
                                <a href="/udyam-registration-procedure">
                                    PROCEDURE
                                </a>
                            </li>
                            <li>
                                <a href="/udyam-registration-benefits">
                                    BENEFITS
                                </a>
                            </li>
                            <li>
                                <a href="/udyam-registration-faqs">
                                    FAQ
                                </a>
                            </li>
                            <li>
                                <a href="/contact-us">
                                    CONTACT US
                                </a>
                            </li>

                            <li>
                                <a href="/about-us">
                                    ABOUT US
                                </a>
                            </li>

                            <li>
                                <a href="/track-application">
                                    TRACK APPLICATION
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 col-sm-6">
                    <div class="footer-widget">
                        <ul class="list-unstyled footer-links">
                            <li>
                                <h4 style="color: #fff;text-transform: capitalize;">Secured by SSL</h4>
                                <img src="/images/ssl.png" width="200px" height="125px">
                            </li>
                            <br/>
                            <li>
                                <h4 style="color: #fff;text-transform: capitalize;">Payment Mode</h4>
                                <img src="/images/payment-mode.png" width="230px" height="160px">
                            </li>
                        </ul>
                    </div>
                </div>
            </div> <!-- / .row -->


            <div class="row text-right pt-5">
                <div class="col-lg-12">
                    <!-- Copyright -->
                    <p class="footer-copy ">
                        &copy; Copyright <span class="current-year">udyamonline.org</span> All rights reserved.
                    </p>
                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </footer>


    <!--  Page Scroll to Top  -->

<!--    <a class="scroll-to-top js-scroll-trigger" href="#top-header">
        <i class="fa fa-angle-up"></i>
    </a>
-->
