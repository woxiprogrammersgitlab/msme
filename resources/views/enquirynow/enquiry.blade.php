@extends('layouts.master')
@section('content')

<!-- Modal -->
<div id="successModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
</div>
<style>
#enq_now_submit_btn_id { 
font-size : 24px;
width : 100%;
color : #fff;
background-color : #5964c6;
border : 1px solid #5964c6;
}

#banner .overlay {
    opacity: 0.7;
}

.overlay {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
}

#cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgb(89, 100, 198, 0.4);
    z-index:9999;
    display:none;
}

@-webkit-keyframes spin {
	from {-webkit-transform:rotate(0deg);}
	to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
	from {transform:rotate(0deg);}
	to {transform:rotate(360deg);}
}

#cover-spin::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}

</style>

<div id="cover-spin"></div>


<section class="banner-area py-5" id="banner" style="background: url('/images/bg/gst_banner_background_1.jpg') no-repeat;">
       <div class="overlay"></div>
        <!-- Content -->
        <div class="container">
            <div class="row ">
            <div class="col-md-8 col-lg-8" style="margin-top:-100px">
                   <div class="banner-content text-lg-left">
                        <h1 style="color:#F7A630;font-size:70px;font-family: 'Times New Roman', Times, serif;line-height: 1em;">
                            Enquire Now
                        </h1>
                        <br/>
                        <br/>
                        <!-- Heading -->
                        <p style="color:#fff;font-size:36px;">Hassle-free Registration, Tax Filing, and Compliance Services in India</p>
                        <!-- Subheading -->
                        <ul style="color:#f2f2f2;font-size:26px;">
                            <li>GST Registration</li>
                            <li>Import Export Code</li>
                            <li>Private Limited Company Registration</li>
                            <li>LLP Registration</li>
                            <li>One Person Company Registration</li>
                            <li>Income Tax Returns</li>
                            <li>ROC Compliance</li>
                            <li>Trust Registration</li>
                            <li>PF/ESIC Registration</li>
                        </ul>
                        <p style="color:#fff;font-size:20px;">
                            <i>We are leading team of professionals, Chartered Accountants, Company Secretaries, Legal Advisors who help in business registration and compliance with the commitment to deliver quality service.</i>
                        </p>
                    </div>
                </div>

            <div class="col-lg-4 ">
                    <div class="banner-contact-form bg-white" style="padding : 20px 10px;">
                    <form id="enquiry_now" class="enquiry_now" action="#" method="POST">
                        {{ csrf_field() }}
                        <!-- Input -->
                        <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                <!--<label class="h6 small d-block text-uppercase">
                                    Your name
                                    <span class="text-danger">*</span>
                                </label>-->

                                <div class="input-group ">
                                    <input class="form-control {{ $errors->has('customer_name') ? 'is-invalid' : ''}}" name="customer_name" id="customer_name" value="{{old('customer_name')}}" required="required" placeholder="Full Name" type="text">
                                    @if($errors->has('customer_name'))
                                    <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('customer_name') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                        <!-- Input -->
                        <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                <!--<label class="h6 small d-block text-uppercase">
                                    Mobile No +91-
                                    <span class="text-danger">*</span>
                                </label>-->

                                <div class="input-group ">
                                    <input class="form-control {{ $errors->has('mobile_number') ? 'is-invalid' : ''}}" name="mobile_number" id="mobile_number" value="{{old('mobile_number')}}" maxlength="10" required="" placeholder="Mobile No +91-" type="text">
                                    @if($errors->has('mobile_number'))
                                    <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile_number') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                        <!-- Input -->
                        <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                <!--<label class="h6 small d-block text-uppercase">
                                    Email ID
                                    <span class="text-danger">*</span>
                                </label>-->

                                <div class="input-group ">
                                    <input class="form-control {{ $errors->has('customer_email') ? 'is-invalid' : ''}}" name="customer_email" id="customer_email" value="{{old('customer_email')}}" required="required" placeholder="Your Email ID" type="email">
                                    @if($errors->has('customer_email'))
                                    <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('customer_email') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                        <!-- Input -->
                         <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                <!--<label class="h6 small d-block text-uppercase">
                                    Issues Related to
                                    <span class="text-danger">*</span>
                                </label>-->

                                <div class="input-group ">
                                    <select required="required" class="form-control {{ $errors->has('enquiry_type_id') ? 'is-invalid' : ''}}" name="enquiry_type_id" id="enquiry_type_id" value="{{old('enquiry_type_id')}}">
                                        <option value="">Please select enquiry type</option>
                                        @foreach ($enquiryTypes as $enquiry)
                                                <option value="{{$enquiry->id}}">{{$enquiry->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('enquiry_type_id'))
                                    <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('enquiry_type_id') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- End Input -->

                    <!-- Input -->
                    <div class="col-sm-12 mb-12">
			
                    <div class="form-group">
                            <!--<p style="color:#000;font-weight:bold;">SECURITY CHECK <span class="text-danger">*</span></p>-->
                            <fieldset style="border: 1px solid #c2c2c2;padding:10px;">
                            <p style="color:#000;">Security Check</p>
                            <div class="input-group">
                                 <label for="mathgroup" style="font-size:28px !important;color:black;">{{ app('mathcaptcha')->label() }} = </label> &nbsp;&nbsp;
                                {!! app('mathcaptcha')->input(['class' => 'form-control', 'id' => 'mathgroup', 'required' => 'required']) !!}
                                @if ($errors->has('mathcaptcha'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mathcaptcha') }}</strong>
                                    </div>
                                @endif
                            </div>
                            </fieldset>
                        </div>
                    </div>
                    <!-- End Input -->
                        
                        <div class="col-sm-12 mb-12">
                            <div class="form-group">
                                <input name="submit" type="submit" class="btn btn-primary" id="enq_now_submit_btn_id" value="Submit" style="font-size:20px;width:100%">
                                <!--<p class="small pt-3">We'll get back to you in 24hrs.</p>-->
                            </div>
                        </div>
                            
                        </form>
                    </div>
                </div>
  
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </section>

@endsection
@section('js')

@endsection

