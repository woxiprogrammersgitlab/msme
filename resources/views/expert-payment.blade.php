@extends('layouts.master')
@section('content')
<style>
.razorpay-payment-button { 
font-size : 24px;
color : #fff;
background-color : #5964c6;
border : 1px solid #5964c6;
}
</style>
<section class="section" id="contact">
    <!-- Content -->
    <div class="container">
	<div class="row  align-items-center">
		<div class="col-md-3 col-lg-3">&nbsp;</div>
            <div class="col-md-6 col-lg-6 text-center text-lg-left" style="color:#000">
		<p>Hello <strong>{{ $data['prefill']['name'] }}</strong>,</p>
		<p>Kindly proceed with payment of consulting fees to confirm your request</p>
		<div style="border : 1px solid #888; padding:20px; background-color:#f2f2f2;text-align:center;">
		<p style="font-size: 20px;color:#5964c6;">Request Number : <strong>{{$orderData['receipt']}}</strong></p>
		<!--  The entire list of Checkout fields is available at
                https://docs.razorpay.com/docs/checkout-form#checkout-fields -->

                <form action="{{URL::to('/expert-payment-success')}}" method="POST">
                    {{ csrf_field() }}
                    <script
                    src="https://checkout.razorpay.com/v1/checkout.js"
                    data-key="<?php echo $data['key']?>"
                    data-amount="<?php echo $data['amount']?>"
                    data-currency="INR"
                    data-name="<?php echo $data['name']?>"
                    data-image="<?php echo $data['image']?>"
                    data-description="<?php echo $data['description']?>"
                    data-prefill.name="<?php echo $data['prefill']['name']?>"
                    data-prefill.email="<?php echo $data['prefill']['email']?>"
                    data-prefill.contact="<?php echo $data['prefill']['contact']?>"
                    data-notes.shopping_order_id="{{$orderData['receipt']}}"
                    data-order_id="<?php echo $data['order_id']?>"
                    <?php if ($displayCurrency !== 'INR') { ?> data-display_amount="<?php echo $data['display_amount']?>" <?php } ?>
                    <?php if ($displayCurrency !== 'INR') { ?> data-display_currency="<?php echo $data['display_currency']?>" <?php } ?>
                    > 
                    </script>
                    <!-- Any extra fields to be submitted with the form but not sent to Razorpay -->
		    <input type="hidden" name="shopping_order_id" value="{{$orderData['receipt']}}">
		</form>
		</div>
	    </div>
		<div class="col-md-3 col-lg-3">&nbsp;</div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>
@endsection
@section('js')
<script>
$(window).load(function() {
	$(".razorpay-payment-button").addClass("blue");
});

$( document ).ready(function() {
	$(".razorpay-payment-button").addClass("blue");
});
$("body").on("load", function() {
	$(".razorpay-payment-button").addClass("blue");
});
</script>
@endsection
