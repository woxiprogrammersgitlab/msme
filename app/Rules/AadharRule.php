<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AadharRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = trim($value);
        $match = '^[0-9]{4}\-[0-9]{4}\-[0-9]{4}$^';

        if (preg_match($match, $value)) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.aadhar');
    }
}
