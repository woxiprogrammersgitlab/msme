<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leads extends Model
{
    protected $table = 'leads';

    protected $fillable = [
        'customer_name', 'mobile_number', 'customer_email',
        'enquiry_type_id', 'lead_type_id'
    ];

    public function getEnquiryTypes()
    {
        return $this->hasOne('App\EnquiryTypes','id','enquiry_type_id');
    }
}
