<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Razorpay\Api\Api;
use App\Components\FlashMessages;
use App\Http\Requests\CaptchaRequest;
use App\Http\Requests\TrackingRequest;
use App\Mail\Enquiry\EnquiryNowAdminMail;
use Illuminate\Support\Facades\Mail;
use App\Leads;
use App\EnquiryTypes;

class EnquiryNowController extends Controller
{
    use FlashMessages;

    public function enquiryNow() {
        $seo = config('seo');
        $seoData = $seo['enquiry-now'];
        $enquiryTypes = EnquiryTypes::all();
        $this->resetCaptchaCode();
        return view('enquirynow.enquiry')->with(compact('seoData','enquiryTypes'));
    }

    public function resetCaptchaCode() {
        app('mathcaptcha')->reset();
    }

    public function enquiryNowSubmit(Request $request){
        $data['customer_name'] = $request->customer_name;
        $data['mobile_number'] = $request->mobile_number;
        $data['customer_email'] = $request->customer_email;
        $data['enquiry_type_id'] = $request->enquiry_type_id;
        $data['lead_type_id'] = 1; //Enquiry Form lead
        $enquirydata = Leads::create($data);

        $this->sendEnquiryMail($enquirydata->id);
        return "Thank you for your interest.. Our executive will get back to you soon.";
    }

    public function sendEnquiryMail($id) {
        $enquiryData = Leads::findOrFail($id);

        /* Send Mail To Admin */
        if(!$enquiryData->admin_enquiry_mail_sent) {
            Mail::to([env('SUPPORT_EMAIL_1')])->send(
                new EnquiryNowAdminMail($enquiryData)
            );
        }
    }
}
