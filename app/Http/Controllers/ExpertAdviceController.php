<?php

namespace App\Http\Controllers;

use App\ExpertAdviceRegistration;
use App\IssueRelatedTos;
use App\Address;
use App\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Razorpay\Api\Api;
use App\Components\FlashMessages;
use App\Http\Requests\CaptchaRequest;
use App\Http\Requests\TrackingRequest;
use App\Mail\ExpertAdviceNewOrderAdminMail;
use App\Mail\ExpertAdviceNewOrderClientMail;
use App\Mail\ExpertAdviceWelcomeOrderClientMail;
use App\Mail\ExpertAdviceWelcomeOrderAdminMail;
use Illuminate\Support\Facades\Mail;
use DateTime;

class ExpertAdviceController extends Controller
{
    use FlashMessages;

    public function expertAdvicePhone() {
        $seo = config('seo');
        $seoData = $seo['expert-advice'];
        $issueRelatedTo = IssueRelatedTos::all();
        $this->resetCaptchaCode();
        $timeSlot = array();
        $todayDate = date('Y-m-d');
        $stTodayDate = $todayDate." 10:00";
        $endTodayDate = $todayDate." 16:00";
        $datetime = new DateTime('tomorrow');
        $nextDayDate =  $datetime->format('Y-m-d');
        $stTomoDate = $nextDayDate." 10:00";
        $endTomoDate = $nextDayDate." 16:00";
        $dataToday = $this->SplitTime($stTodayDate, $endTodayDate, "60");
        $dataTomo = $this->SplitTime($stTomoDate, $endTomoDate, "60");
        $timeSlot = array_merge($dataToday, $dataTomo);
        return view('expertadvice.expert')->with(compact('seoData','issueRelatedTo','timeSlot'));
    }

    public function SplitTime($StartTime, $EndTime, $Duration="60"){
        $ReturnArray = array ();// Define output
        $StartTime    = strtotime ($StartTime); //Get Timestamp
        $EndTime      = strtotime ($EndTime); //Get Timestamp
        $currTime = strtotime (date('Y-m-d H:i'));

        $AddMins  = $Duration * 120;

        while ($StartTime <= $EndTime) //Run loop
        {
            if ( $currTime  < $EndTime ) {
                $ReturnArray[] = date ("dS M Y G:i", $StartTime)." - ".date ("G:i", $StartTime + $AddMins);
            }
            $StartTime += $AddMins; //Endtime check
        }
        return $ReturnArray;
    }

    public function registerCustomerForExpertAdvice(Request $request) {
        //Get Post Data
        $data['customer_name'] = $request->customer_name;
        $data['mobile_number'] = $request->mobile_number;
        $data['customer_email'] = $request->customer_email;
        $data['issue_related_to_id'] = $request->issue_related_to_id;
        $data['other_issue'] = $request->other_issue;
        $data['customer_message'] = $request->customer_message;
        $data['preferable_slot'] = $request->preferable_slot;
        $data['fees'] = 0;

        $custRegData = ExpertAdviceRegistration::create($data);

        $custRegNumber = $this->createExpertAdviceRegNumber($custRegData->id);
        $custRegData->expert_advice_cust_reg_number = $custRegNumber;
        $custRegData->save();
        
        //Send Welcome Email with payment link
        $this->sendWelcomeMail($custRegData->id);

        //return order ID
        $keyId = env('RAZORPAY_ACCESS_KEY_ID');
        $keySecret = env('RAZORPAY_SECRET_ACCESS_KEY');
	    $displayCurrency = 'INR';
	    $reg_amt = env('EXPERT_ADVICE_AMOUNT');
        $amount = $reg_amt*100;
        $api = new Api($keyId, $keySecret); //keyId, keySecret
        $orderId  = $api->order->create([
            'receipt'         =>  $custRegNumber,
            'amount'          =>  $amount, // amount in the smallest currency unit
            'currency'        =>  'INR',// <a href="/docs/payment-gateway/payments/international-payments/#supported-currencies" target="_blank">See the list of supported currencies</a>.)
            'payment_capture' =>  '0'
        ]);
        
        $custdata['order_id'] = $orderId['id'];
        $custdata['key'] = $keyId;
        $custdata['amount'] = $amount;
        $custdata['currency'] = "INR";
        $custdata['name'] = "Udyamonline.org";
        $custdata['description'] = "Consultation Fee";
        $custdata['image'] = env('APP_URL')."/images/udyam-razor-logo.png";
        $custdata['email'] = $request->customer_email;
        $custdata['contact'] = $request->mobile_number;
        $custdata['cust_name'] = $request->customer_name;
        $custdata['address'] = "Address";
        $custdata['merchant_order_id'] = $custRegData->expert_advice_cust_reg_number;
        return $custdata;
    }

    public function sendWelcomeMail($id) {
        $custRegData = ExpertAdviceRegistration::findOrFail($id);
        $applicationPaymentUrl = route('payment-expert-advice', ['id' => encrypt($id)]);

        /* Send Mail To Client */
        if(!$custRegData->client_welcome_mail_sent) {
            Mail::to($custRegData->customer_email)->send(
                new ExpertAdviceWelcomeOrderClientMail($custRegData, $applicationPaymentUrl)
            );
        }

        /* Send Mail To Admin */
        if(!$custRegData->admin_welcome_mail_sent) {
            Mail::to([env('SUPPORT_EMAIL_1')])->send(
                new ExpertAdviceWelcomeOrderAdminMail($custRegData)
            );
        }
    }

    public function sendNewMail($id) {
        $custRegData = ExpertAdviceRegistration::findOrFail($id);

        /* Send Mail To Client */
        if(!$custRegData->client_payment_mail_sent) {
            Mail::to($custRegData->customer_email)->send(
                new ExpertAdviceNewOrderClientMail($custRegData)
            );
        }

        /* Send Mail To Admin */
        if(!$custRegData->admin_payment_mail_sent) {
            Mail::to([env('SUPPORT_EMAIL_1')])->send(
                new ExpertAdviceNewOrderAdminMail($custRegData)
            );
        }
    }

    public function paymentExpertAdvice($id) {
        $id = decrypt($id);
        $custRegData = ExpertAdviceRegistration::findOrFail($id);
        $keyId = env('RAZORPAY_ACCESS_KEY_ID');
        $keySecret = env('RAZORPAY_SECRET_ACCESS_KEY');
	    $displayCurrency = 'INR';
	    $reg_amt = env('EXPERT_ADVICE_AMOUNT');
        $amount = $reg_amt*100;
        $api = new Api($keyId, $keySecret); //keyId, keySecret
        $orderData = [
            'receipt'         => $custRegData->expert_advice_cust_reg_number,
            'amount'          => $amount, // rupees in paise
            'currency'        => 'INR',
            'payment_capture' => 1 // auto capture
        ];

        $razorpayOrder = $api->order->create($orderData);
        $razorpayOrderId = $razorpayOrder['id'];
        $checkout = 'automatic';
        $data = [
            "key"               => $keyId,
            "amount"            => $amount,
            "name"              => "Udyamonline.org",
            "description"       => "Consultation Fee",
            "image"             => env('APP_URL')."/images/udyam-razor-logo.png",
            "prefill"           => [
            "name"              => $custRegData->customer_name,
            "email"             => $custRegData->customer_email,
            "contact"           => $custRegData->mobile_number,
            ],
            "notes"             => [
            "address"           => "address",
            "merchant_order_id" => $custRegData->expert_advice_cust_reg_number,
            ],
            "theme"             => [
                "color"             => '#5964C6',
            ],
            "order_id"          => $razorpayOrderId,
        ];

        return view('expert-payment')->with(compact('data','displayCurrency','orderData'));
    }

    public function paymentSucess(Request $request) {
        $orderId = $request->shopping_order_id;
        $custRegData = ExpertAdviceRegistration::where('expert_advice_cust_reg_number',$orderId)->first();
        $customer_name = $custRegData->customer_name;
        $timeslot = $custRegData->preferable_slot;
        $custRegData->status = true;
        $custRegData->payment_gateway_data = json_encode($request->all());
        $custRegData->save();
        $data = [
            'mobile' => $custRegData->mobile_number,
            'order' => $orderId
        ];
        //$applicationTrackUrl = route('track-application', ['application_number' => encrypt($data)]);
	    self::success('Payment Successful');

        $this->sendNewMail($custRegData->id);

        return view('expert-payment-success')->with(compact('orderId', 'customer_name','timeslot'));
    }

    public function getPaymentSucess($id) {
        $orderId = $id;
        $custRegData = ExpertAdviceRegistration::where('expert_advice_cust_reg_number',$orderId)->first();
        $customer_name = $custRegData->customer_name;
        $timeslot = $custRegData->preferable_slot;
	    self::success('Payment Successful');
        return view('expert-payment-success')->with(compact('orderId', 'customer_name','timeslot'));
    }

    public function paymentSucessIndirect(Request $request) {
        $orderId = $request->shopping_order_id;
        $custRegData = ExpertAdviceRegistration::where('expert_advice_cust_reg_number',$orderId)->first();
        $customer_name = $custRegData->customer_name;
        $custRegData->status = true;
        $custRegData->payment_gateway_data = json_encode($request->all());
        $custRegData->save();
        $this->sendNewMail($custRegData->id);
        return $orderId;
    }


    public function mailEA()
    {
        $custRegData = ExpertAdviceRegistration::find(1);
         //Mail::to($aadharData->email)->send(
          //return new ExpertAdviceWelcomeOrderClientMail($aadharData,'');
          return new ExpertAdviceNewOrderClientMail($custRegData);
        // );
        //return new ExpertAdviceNewOrderAdminMail($aadharData);
    }

    public function validateCaptcha(CaptchaRequest $request) {
        return true;
    }

    public function resetCaptchaCode() {
        app('mathcaptcha')->reset();
    }

    public function createExpertAdviceRegNumber($id) {
        $now = now();
        $random = $this->randomDigits();
        return "XCB".$now->format('ymd').$random.$id;
    }

}
