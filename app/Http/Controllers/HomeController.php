<?php

namespace App\Http\Controllers;

use App\AadharRegistration;
use App\Address;
use App\ContactUs;
use App\Http\Requests\ContactUsRequest;
use App\Http\Requests\UdyamFormRequest;
use App\Http\Requests\MsmeFormRequest;
use App\NicCode;
use App\OrganizationType;
use App\SocialCategory;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Razorpay\Api\Api;
use App\Components\FlashMessages;
use App\Http\Requests\CaptchaRequest;
use App\Http\Requests\TrackingRequest;
use App\Mail\NewOrderAdminMail;
use App\Mail\NewOrderClientMail;
use App\Mail\WelcomeOrderClientMail;
use App\Mail\WelcomeOrderAdminMail;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    use FlashMessages;

    public function index() {
        for($index = 1; $index <= 38; $index++) {
            $nicId1[] = $index;
        }
        for($index = 39; $index <= 80; $index++) {
            $nicId2[] = $index;
        }

        $nicCodes1 = NicCode::whereIn('id',$nicId1)->get();
        $nicCodes2 = NicCode::whereIn('id',$nicId2)->get();
        $socialCategories = SocialCategory::all();
        $organizationTypes = OrganizationType::all();
        $states = State::all();
        $seo = config('seo');
        $seoData = $seo['udyam_registration'];
        $this->resetCaptchaCode();
        return view('index')->with(compact('seoData','nicCodes1','nicCodes2','socialCategories','organizationTypes','states'));
    }

    public function msmeToUdyamRegistration() {
        for($index = 1; $index <= 38; $index++) {
            $nicId1[] = $index;
        }
        for($index = 39; $index <= 80; $index++) {
            $nicId2[] = $index;
        }

        $nicCodes1 = NicCode::whereIn('id',$nicId1)->get();
        $nicCodes2 = NicCode::whereIn('id',$nicId2)->get();
        $socialCategories = SocialCategory::all();
        $organizationTypes = OrganizationType::all();
        $states = State::all();
        $seo = config('seo');
        $seoData = $seo['msme_registration'];
        $this->resetCaptchaCode();
        return view('msme-to-udyam')->with(compact('seoData','nicCodes1','nicCodes2','socialCategories','organizationTypes','states'));
    }

    public function payment($id) {
        $id = decrypt($id);
        $aadharData = AadharRegistration::findOrFail($id);
        $keyId = env('RAZORPAY_ACCESS_KEY_ID');
        $keySecret = env('RAZORPAY_SECRET_ACCESS_KEY');
	    $displayCurrency = 'INR';
	    $reg_amt = env('REGISTRATION_AMOUNT');
        $amount = $reg_amt*100;
        $api = new Api($keyId, $keySecret); //keyId, keySecret
        $orderData = [
            'receipt'         => $aadharData->application_number,
            'amount'          => $amount, // rupees in paise
            'currency'        => 'INR',
            'payment_capture' => 1 // auto capture
        ];

        $razorpayOrder = $api->order->create($orderData);
        $razorpayOrderId = $razorpayOrder['id'];
        $checkout = 'automatic';
        $data = [
            "key"               => $keyId,
            "amount"            => $amount,
            "name"              => "Udyamonline.org",
            "description"       => "Registration Fee",
            "image"             => env('APP_URL')."/images/udyam-razor-logo.png",
            "prefill"           => [
            "name"              => $aadharData->entrepreneur_name,
            "email"             => $aadharData->email,
            "contact"           => $aadharData->mobile,
            ],
            "notes"             => [
            "address"           => "address",
            "merchant_order_id" => $aadharData->id,
            ],
            "theme"             => [
                "color"             => '#5964C6',
            ],
            "order_id"          => $razorpayOrderId,
        ];

        return view('payment')->with(compact('data','displayCurrency','orderData'));
    }

    public function paymentSucess(Request $request) {
        $orderId = $request->shopping_order_id;
        $aadharData = AadharRegistration::where('application_number',$orderId)->first();
        $customer_name = $aadharData->entrepreneur_name;
        $aadharData->status = true;
        $aadharData->payment_gateway_data = json_encode($request->all());
        $aadharData->save();
        $data = [
            'mobile' => $aadharData->mobile,
            'order' => $orderId
        ];
        $applicationTrackUrl = route('track-application', ['application_number' => encrypt($data)]);
	    self::success('Payment Successful');

        /* Send Mail To Client */
        if(!$aadharData->client_payment_mail_sent) {
            Mail::to($aadharData->email)->send(
                new NewOrderClientMail($aadharData, $applicationTrackUrl)
            );
        }

        /* Send Mail To Admin */
        if(!$aadharData->admin_payment_mail_sent) {
            Mail::to([env('SUPPORT_EMAIL_1')])->send(
                new NewOrderAdminMail($aadharData)
            );
        }

        return view('payment-success')->with(compact('orderId','applicationTrackUrl','customer_name'));
    }

    public function msmeRegistrationSubmit(MsmeFormRequest $request) {
        $id = $this->registrationFormSubmit($request, 2); // 2 For Msme to Udyam Registration Form
        $aadharData = AadharRegistration::findOrFail($id);
        $applicationPaymentUrl = route('payment', ['id' => encrypt($id)]);

        /* Send Mail To Client */
        if(!$aadharData->client_welcome_mail_sent) {
            Mail::to($aadharData->email)->send(
                new WelcomeOrderClientMail($aadharData, $applicationPaymentUrl)
            );
        }

        /* Send Mail To Admin */
        if(!$aadharData->admin_welcome_mail_sent) {
            Mail::to([env('SUPPORT_EMAIL_1')])->send(
                new WelcomeOrderAdminMail($aadharData)
            );
        }

        $this->resetCaptchaCode();
        return redirect()->route('payment', ['id' => encrypt($id)]);
    }

    public function registerMSMEAjax(Request $request) {
       
        $data['form_type'] = $request->formType;
        $id = $this->registrationFormSubmit($request, $request->formType); 
        $aadharData = AadharRegistration::findOrFail($id);
        $app_number = $aadharData->application_number;
        
        //Send Welcome Email with payment link
        $this->sendWelcomeMail($aadharData->id);

        //return order ID
        $keyId = env('RAZORPAY_ACCESS_KEY_ID');
        $keySecret = env('RAZORPAY_SECRET_ACCESS_KEY');
	    $displayCurrency = 'INR';
	    $reg_amt = env('REGISTRATION_AMOUNT');
        $amount = $reg_amt*100;
        $api = new Api($keyId, $keySecret); //keyId, keySecret
        $orderId  = $api->order->create([
            'receipt'         =>  $app_number,
            'amount'          =>  $amount, // amount in the smallest currency unit
            'currency'        =>  'INR',// <a href="/docs/payment-gateway/payments/international-payments/#supported-currencies" target="_blank">See the list of supported currencies</a>.)
            'payment_capture' =>  '0'
        ]);
        
        $custdata['order_id'] = $orderId['id'];
        $custdata['key'] = $keyId;
        $custdata['amount'] = $amount;
        $custdata['currency'] = "INR";
        $custdata['name'] = "Udyamonline.org";
        $custdata['description'] = "Registration Fee";
        $custdata['image'] = env('APP_URL')."/images/udyam-razor-logo.png";
        $custdata['email'] = $aadharData->email;
        $custdata['contact'] = $aadharData->mobile;
        $custdata['cust_name'] = $aadharData->entrepreneur_name;
        $custdata['address'] = "Address";
        $custdata['merchant_order_id'] = $aadharData->application_number;
        return $custdata;
    }

    public function getpaymentSucess($id) {
        $orderId = $id;
        $aadharData = AadharRegistration::where('application_number',$orderId)->first();
        $customer_name = $aadharData->entrepreneur_name;
        $data = [
            'mobile' => $aadharData->mobile,
            'order' => $orderId
        ];
        $applicationTrackUrl = route('track-application', ['application_number' => encrypt($data)]);
	    self::success('Payment Successful');
        return view('payment-success')->with(compact('orderId','applicationTrackUrl','customer_name'));
    }

    public function paymentMSMESucessIndirect(Request $request) {
        $orderId = $request->shopping_order_id;
        $aadharData = AadharRegistration::where('application_number',$orderId)->first();
        $customer_name = $aadharData->entrepreneur_name;
        $aadharData->status = true;
        $aadharData->payment_gateway_data = json_encode($request->all());
        $aadharData->save();
        $this->sendNewMail($aadharData->id);
        return $orderId;
    }

    public function sendWelcomeMail($id) {
        $aadharData = AadharRegistration::findOrFail($id);
        $applicationPaymentUrl = route('payment', ['id' => encrypt($id)]);

        /* Send Mail To Client */
        if(!$aadharData->client_welcome_mail_sent) {
            Mail::to($aadharData->email)->send(
                new WelcomeOrderClientMail($aadharData, $applicationPaymentUrl)
            );
        }

        /* Send Mail To Admin */
        if(!$aadharData->admin_welcome_mail_sent) {
            Mail::to([env('SUPPORT_EMAIL_1')])->send(
                new WelcomeOrderAdminMail($aadharData)
            );
        }
    }

    public function sendNewMail($id) {
        $aadharData = AadharRegistration::findOrFail($id);
        $orderId = $aadharData->application_number;
        $data = [
            'mobile' => $aadharData->mobile,
            'order' => $orderId
        ];
        $applicationTrackUrl = route('track-application', ['application_number' => encrypt($data)]);
        /* Send Mail To Client */
        if(!$aadharData->client_payment_mail_sent) {
            Mail::to($aadharData->email)->send(
                new NewOrderClientMail($aadharData, $applicationTrackUrl)
            );
        }

        /* Send Mail To Admin */
        if(!$aadharData->admin_payment_mail_sent) {
            Mail::to([env('SUPPORT_EMAIL_1')])->send(
                new NewOrderAdminMail($aadharData)
            );
        }
    }


    public function udyamRegistrationSubmit(UdyamFormRequest $request) {
        $id = $this->registrationFormSubmit($request, 1); //1 For Udyam Registration Form
        $aadharData = AadharRegistration::findOrFail($id);
        $applicationPaymentUrl = route('payment', ['id' => encrypt($id)]);
        //$applicationPaymentUrl = env('APP_URL')."/payment/".encrypt($id);
        //$aadharData->paymentUrl = $applicationPaymentUrl;
        /* Send Mail To Client */
        if(!$aadharData->client_welcome_mail_sent) {
            Mail::to($aadharData->email)->send(
                new WelcomeOrderClientMail($aadharData, $applicationPaymentUrl)
            );
        }

        /* Send Mail To Client */
        if(!$aadharData->admin_welcome_mail_sent) {
            Mail::to([env('SUPPORT_EMAIL_1')])->send(
                new WelcomeOrderAdminMail($aadharData)
            );
        }
        $this->resetCaptchaCode();
        return redirect()->route('payment', ['id' => encrypt($id)]);
    }

    public function registrationFormSubmit(Request $request, $formType) {
        $data = $request->all();
        $data['form_type'] = $formType;

        if($data['business_activity_type'] == 'Manufacturer') {
            $data['nic_code_id'] = $data['nic_code_id'];
        } else if($data['business_activity_type'] == 'Service provider') {
            $data['nic_code_id'] = $data['nic_code_id1'];
        }

        if ($request->hasFile('aadhar_image')) {
            $imageName = strtotime(now()).$this->randomDigits(5).'.'.request()->aadhar_image->getClientOriginalExtension();
            $path = public_path('images/client');
            if(Storage::exists($path)) {
                Storage::makeDirectory($path);
            }

            $path = Storage::putFileAs(
                $path, $request->file('aadhar_image'), $imageName
            );
            $data['aadhar_image'] = $imageName;
        }

        if ($request->hasFile('aadhar_image_existing')) {
            $imageName = strtotime(now()).$this->randomDigits(5).'.'.request()->aadhar_image_existing->getClientOriginalExtension();
            $path = public_path('images/client');
            if(Storage::exists($path)) {
                Storage::makeDirectory($path);
            }

            $path = Storage::putFileAs(
                $path, $request->file('aadhar_image_existing'), $imageName
            );
            $data['aadhar_image_existing'] = $imageName;
        }

        $aadharData = AadharRegistration::create($data);
        $data['aadhar_registration_id'] = $aadharData->id;
        if($data['is_office_address_same'] == 'no') {
            $data['is_office_address_same'] = false;
        } elseif($data['is_office_address_same'] == 'yes') {
            $data['is_office_address_same'] = true;
        }

        Address::create($data);
        if(!$data['is_office_address_same']) {
            $address['house_number'] = $data['house_number1'];
            $address['premise'] = $data['premise1'];
            $address['road'] = $data['road1'];
            $address['area'] = $data['area1'];
            $address['city'] = $data['city1'];
            $address['pin'] = $data['pin1'];
            $address['state_id'] = $data['state_id1'];
            $address['aadhar_registration_id'] = $data['aadhar_registration_id'];
            $address['district'] = $data['district1'];
            $address['is_office_address_same'] = $data['is_office_address_same'];
            Address::create($address);
        }

	    $aadharData->fees = 0;

        /* Create Application ID */
        $applicationNumber = $this->createApplicationNumber($aadharData->id);
        $aadharData->application_number = $applicationNumber;
        $aadharData->save();

        return $aadharData->id;
    }

    public function createApplicationNumber($id) {
        $now = now();
        $random = $this->randomDigits();
        return "UDM".$now->format('ymd').$random.$id;
    }

    public function trackApplication(Request $request) {
        $seo = config('seo');
        $seoData = $seo['track_application'];
        if($request->has('application_number')) {
            $applicationNumber = decrypt($request->application_number);
            $data = AadharRegistration::where('mobile',$applicationNumber['mobile'])->where('application_number', $applicationNumber['order'])->first();
            if(is_null($data)) {
                self::danger('No record found!');
            } else{
                self::success('Record found!');
            }
            return view('track-application')->with(compact('data','seoData'));
        }
        return view('track-application')->with(compact('seoData'));;
    }

    public function trackApplicationSubmit(TrackingRequest $request) {
        $data = [
            'mobile' => $request->mobile,
            'order' => $request->order_number
        ];
        $this->resetCaptchaCode();
        return redirect()->route('track-application', ['application_number' => encrypt($data)]);
    }

    public function contactUsSubmit(ContactUsRequest $request) {
        /*
        https://stackoverflow.com/questions/21004310/in-laravel-the-best-way-to-pass-different-types-of-flash-messages-in-the-sessio
        self::message('info', 'Just a plain message.');
        self::message('success', 'Item has been added.');
        self::message('warning', 'Service is currently under maintenance.');
        self::message('danger', 'An unknown error occured.');

        //or

        self::info('Just a plain message.');
        self::success('Item has been added.');
        self::warning('Service is currently under maintenance.');
        self::danger('An unknown error occured.');
        */
        self::success('Thanks for contacting us, our team will get back to you as soon as possible.');
        $data = $request->all();
        $contact = ContactUs::create($data);
        $this->resetCaptchaCode();
        return redirect()->route('contact-us');
    }

    public function mail()
    {
        $aadharData = AadharRegistration::find(1);
         //Mail::to($aadharData->email)->send(
          return   new NewOrderClientMail($aadharData,'');
        // );
        //return new NewOrderAdminMail($aadharData);
    }

    public function faqs() {
        $seo = config('seo');
        $seoData = $seo['faq'];
        return view('faqs')->with(compact('seoData'));
    }

    public function procedure() {
        $seo = config('seo');
        $seoData = $seo['procedure'];
        return view('procedure')->with(compact('seoData'));
    }

    public function certificate() {
        $seo = config('seo');
        $seoData = $seo['certificate'];
        return view('certificate')->with(compact('seoData'));
    }

    public function aboutUs() {
        $seo = config('seo');
        $seoData = $seo['about'];
        return view('about')->with(compact('seoData'));
    }

    public function contactUs() {
        $seo = config('seo');
        $seoData = $seo['contact'];

        return view('contact')->with(compact('seoData'));
    }

    public function benefits() {
        $seo = config('seo');
        $seoData = $seo['benifits'];
        return view('benefits')->with(compact('seoData'));
    }

    public function termsAndCondition() {
	$seo = config('seo');
        $seoData = $seo['tnc'];
        return view('terms-and-condition')->with(compact('seoData'));
    }

    public function privacyPolicy() {
	$seo = config('seo');
        $seoData = $seo['privacy'];
        return view('privacy-policy')->with(compact('seoData'));
    }

    public function refundPolicy() {
	$seo = config('seo');
        $seoData = $seo['refund'];
        return view('refund-policy')->with(compact('seoData'));
    }

    public function home() {
        $seo = config('seo');
        $seoData = $seo['home'];
        return view('home')->with(compact('seoData'));
    }

    public function validateCaptcha(CaptchaRequest $request) {
        return true;
    }

    public function resetCaptchaCode() {
        app('mathcaptcha')->reset();
    }
}

