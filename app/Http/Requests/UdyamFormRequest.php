<?php

namespace App\Http\Requests;

use App\Rules\AadharRule;
use App\Rules\AlphaNum;
use App\Rules\AlphaNumSpace;
use App\Rules\AlphaSpace;
use App\Rules\GstRule;
use App\Rules\IfscRule;
use App\Rules\MobileRule;
use App\Rules\PanCardRule;
use App\Rules\PinRule;
use Illuminate\Foundation\Http\FormRequest;

class UdyamFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'aadhar_number' => ['required', new AadharRule],
            'entrepreneur_name' => ['required',new AlphaSpace,'max:255'],
            'gender' => 'required',
            'is_handicapped' => 'required',
            'business_name' => ['required',new AlphaSpace,'max:50'],
            'organization_type_id' => 'required',
            'pan_number' => ['required',new PanCardRule],
            'house_number' => ['required',new AlphaNumSpace,'max:255'],
            'premise' => ['required',new AlphaNumSpace,'max:255'],
            'road' => ['required',new AlphaNumSpace,'max:255'],
            'area' => ['required',new AlphaNumSpace,'max:255'],
            'city' => ['required',new AlphaSpace,'max:255'],
            'pin' => ['required', new PinRule],
            'state_id' => 'required',
            'district' => ['required',new AlphaSpace,'max:255'],
            'is_office_address_same' => 'required',
            'mobile' => ['required', new MobileRule],
            'email' => 'required|email|max:255',
            'commencement_date' => 'required',
            //'bank_account_number' => 'required|integer|digits_between:10,20',
            'ifsc_code' => ['required',new IfscRule],
            'business_activity_type' => 'required',
            'about_business' => ['required',new AlphaNumSpace,'max:255'],
            'employee_count' => 'required|integer',
            'investment_in_plant' => 'required',
            'aadhar_image' => 'nullable|mimes:jpeg,pdf,png|max:5120',
            'gstin_number' => ['nullable', new GstRule],
            'is_gem' => 'required',
            'is_treds' => 'required',
            't_and_c' => 'required',
            'mathcaptcha' => 'required|mathcaptcha',
        ];

        if(request()->is_office_address_same == 'no') {
            $rules['house_number1'] = ['required',new AlphaNumSpace,'max:255'];
            $rules['premise1'] = ['required',new AlphaNumSpace,'max:255'];
            $rules['road1'] = ['required',new AlphaNumSpace,'max:255'];
            $rules['area1'] = ['required',new AlphaNumSpace,'max:255'];
            $rules['city1'] = ['required',new AlphaSpace,'max:255'];
            $rules['pin1'] = ['required', new PinRule];
            $rules['state_id1'] = 'required';
            $rules['district1'] = ['required',new AlphaSpace,'max:255'];
        }

        if(request()->business_activity_type == 'Manufacturer') {
            $rules['nic_code_id'] = 'required';
        } elseif(request()->business_activity_type == 'Service provider') {
            $rules['nic_code_id1'] = 'required';
        }

        return $rules;
    }
}
