<?php

namespace App\Http\Requests;

use App\Rules\AlphaNumSpace;
use App\Rules\AlphaSpace;
use App\Rules\MobileRule;
use Illuminate\Foundation\Http\FormRequest;

class ContactUsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required',new AlphaSpace,'max:255'],
            'mobile' => ['required', new MobileRule],
            'email' => 'required|email|max:255',
            'message' => ['nullable',new AlphaNumSpace,'max:3000'],
            'mathcaptcha' => 'required|mathcaptcha',
        ];
    }
}
