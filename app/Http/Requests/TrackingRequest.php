<?php

namespace App\Http\Requests;

use App\Rules\MobileRule;
use App\Rules\OrderNumberRule;
use Illuminate\Foundation\Http\FormRequest;

class TrackingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile' => ['required', new MobileRule],
            'order_number' => ['required',new OrderNumberRule],
            'mathcaptcha' => 'required|mathcaptcha'
        ];
    }
}
