<?php

namespace App\Mail;

use App\ExpertAdviceRegistration;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ExpertAdviceWelcomeOrderClientMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The ExpertAdviceRegistration instance.
     *
     * @var ExpertAdviceRegistration
    */
    public $custRegData;

    public $applicationPaymentUrl;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ExpertAdviceRegistration $custRegData, $applicationPaymentUrl)
    {
        $this->custRegData = $custRegData;
        $this->applicationPaymentUrl = $applicationPaymentUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = $this->custRegData->expert_advice_cust_reg_number;
        $this->custRegData->client_welcome_mail_sent = true;
        $this->custRegData->save();
        return $this->view('emails.client.expert-welcome-order')
                    ->subject('(Action Required) Request No : '.$id." Expert Call Back Service");
    }
}
