<?php

namespace App\Mail;

use App\ExpertAdviceRegistration;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ExpertAdviceNewOrderClientMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The ExpertAdviceRegistration instance.
     *
     * @var ExpertAdviceRegistration
    */
    public $custRegData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ExpertAdviceRegistration $custRegData)
    {
        $this->custRegData = $custRegData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = $this->custRegData->expert_advice_cust_reg_number;
        $this->custRegData->client_payment_mail_sent = true;
        $this->custRegData->fees = env('EXPERT_ADVICE_AMOUNT');
        $this->custRegData->save();
        return $this->view('emails.client.expert-new-order')
                    ->subject('(Success) Request No : '.$id." Expert Call Back Service - Payment Status Approved");
    }
}
