<?php

namespace App\Mail;

use App\AadharRegistration;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class WelcomeOrderAdminMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The AadharRegistration instance.
     *
     * @var AadharRegistration
    */
    public $aadharData;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AadharRegistration $aadharData)
    {
        $this->aadharData = $aadharData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = $this->aadharData->application_number;
        $this->aadharData->admin_welcome_mail_sent = true;
        $this->aadharData->save();

        if(!is_null($this->aadharData->aadhar_image) && !is_null($this->aadharData->aadhar_image_existing)) {
            $path1 = public_path('images/client')."/".$this->aadharData->aadhar_image;
            $path2 = public_path('images/client')."/".$this->aadharData->aadhar_image_existing;
            return $this->view('emails.admin.welcome-order')
            ->attachFromStorage($path1)
            ->attachFromStorage($path2)
            ->subject('UDYAM Registration : '.$id." (Status : Pending)");
        } else if(!is_null($this->aadharData->aadhar_image)) {
            $path = public_path('images/client')."/".$this->aadharData->aadhar_image;
            return $this->view('emails.admin.welcome-order')
            ->attachFromStorage($path)
            ->subject('UDYAM Registration : '.$id." (Status : Pending)");
        } else if(!is_null($this->aadharData->aadhar_image_existing)) {
            $path = public_path('images/client')."/".$this->aadharData->aadhar_image_existing;
            return $this->view('emails.admin.welcome-order')
            ->attachFromStorage($path)
            ->subject('UDYAM Registration : '.$id." (Status : Pending)");
        } else {
            return $this->view('emails.admin.welcome-order')
            ->subject('UDYAM Registration : '.$id." (Status : Pending)");
        }
    }
}
