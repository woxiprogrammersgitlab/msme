<?php

namespace App\Mail\Enquiry;

use App\Leads;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class EnquiryNowAdminMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The Leads instance.
     *
     * @var Leads
    */
    public $enquiryData;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Leads $enquiryData)
    {
        $this->enquiryData = $enquiryData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = $this->enquiryData->id;
        $service = $this->enquiryData->getEnquiryTypes->name;
        $this->enquiryData->admin_enquiry_mail_sent = true;
        $this->enquiryData->save();

        return $this->view('emails.enquiry.enquiry-mail-to-admin')
            ->subject('Lead Generated For '.$service.' Service');

    }
}
