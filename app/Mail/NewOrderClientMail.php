<?php

namespace App\Mail;

use App\AadharRegistration;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class NewOrderClientMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The AadharRegistration instance.
     *
     * @var AadharRegistration
    */
    public $aadharData;

    public $applicationTrackUrl;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AadharRegistration $aadharData, $applicationTrackUrl)
    {
        $this->aadharData = $aadharData;
        $this->applicationTrackUrl = $applicationTrackUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = $this->aadharData->application_number;
        $this->aadharData->client_payment_mail_sent = true;
        $this->aadharData->fees = env('REGISTRATION_AMOUNT');
        $this->aadharData->save();
        return $this->view('emails.client.new-order')
                    ->subject('UDYAM Registration : '.$id." (Status : Payment Successful)");
    }
}
