<?php

namespace App\Mail;

use App\AadharRegistration;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class WelcomeOrderClientMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The AadharRegistration instance.
     *
     * @var AadharRegistration
    */
    public $aadharData;

    public $applicationPaymentUrl;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AadharRegistration $aadharData, $applicationPaymentUrl)
    {
        $this->aadharData = $aadharData;
        $this->applicationPaymentUrl = $applicationPaymentUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = $this->aadharData->application_number;
        $this->aadharData->client_welcome_mail_sent = true;
        $this->aadharData->save();
        return $this->view('emails.client.welcome-order')
                    ->subject('UDYAM Registration : '.$id." (Status : Pending)");
    }
}
