<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpertAdviceRegistration extends Model
{
    protected $table = 'expert_advice';

    protected $fillable = [
        'customer_name', 'mobile_number', 'customer_email',
        'customer_message', 'issue_related_to_id', 'preferable_slot',
        'other_issue','expert_advice_cust_reg_number','payment_gateway_data',
        'status'
    ];

    public function issueRelatedTosType()
    {
        return $this->hasOne('App\IssueRelatedTos','id','issue_related_to_id');
    }
}
