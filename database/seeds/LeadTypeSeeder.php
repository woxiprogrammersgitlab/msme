<?php

use Illuminate\Database\Seeder;
use App\LeadTypes;

class LeadTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();
        $data = [
            [
                'name' => 'Enquiry Now Page',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ]
        ];
        LeadTypes::insert($data);
    }
}
