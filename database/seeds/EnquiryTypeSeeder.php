<?php

use Illuminate\Database\Seeder;
use App\EnquiryTypes;

class EnquiryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();
        $data = [
            [
                'name' => 'GST Registration',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Import Export Code',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Private Limited Company Registration',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'LLP Registration',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'One Person Company Registration',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Income Tax Returns',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'ROC Compliance',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Trust Registration',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'PF/ESIC Registration',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Other Compliance',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
        ];
        EnquiryTypes::insert($data);
    }
}
