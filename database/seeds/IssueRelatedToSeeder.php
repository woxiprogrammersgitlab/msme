<?php

use App\IssueRelatedTos;
use Illuminate\Database\Seeder;

class IssueRelatedToSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();
        $data = [
            [
                'name' => 'Others? Please specify',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Registration Process under MSME?',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'How to download MSME/Udyam certificate?',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'What is government fee for MSME/Udyam Registration?',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'How is the value of plant & machinery computed?',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Benefits under MSME (please specify what you are looking for)?',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'What is Government E Market (GeM)?',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'How to make changes in Existing MSME/Udyam Registration?',
                'description' => null,
                'is_active' => TRUE,
                'created_at' => $now,
                'updated_at' => $now
            ],
        ];
        IssueRelatedTos::insert($data);
    }
}
