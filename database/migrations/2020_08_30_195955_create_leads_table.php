<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->string('customer_name',255)->nullable();
            $table->string('mobile_number',255)->nullable();
            $table->string('customer_email',255)->nullable();
            $table->unsignedBigInteger('enquiry_type_id')->nullable();
            $table->unsignedBigInteger('lead_type_id')->nullable();
            $table->boolean('admin_enquiry_mail_sent')->default(false);  
            $table->timestamps();
        });

        Schema::table('leads', function($table) {
            $table->foreign('enquiry_type_id')->references('id')->on('enquiry_types')->onUpdate('cascade');
            $table->foreign('lead_type_id')->references('id')->on('lead_types')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
