<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpertAdviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expert_advice', function (Blueprint $table) {
            $table->id();
            $table->string('customer_name',255)->nullable();
            $table->string('mobile_number',255)->nullable();
            $table->string('customer_email',255)->nullable();
            $table->text('customer_message')->nullable();
            $table->unsignedBigInteger('issue_related_to_id')->nullable();
            $table->string('preferable_slot',255)->nullable();
            $table->text('other_issue')->nullable();
            $table->string('expert_advice_cust_reg_number',255)->index()->nullable();
            $table->text('payment_gateway_data')->nullable();
            $table->boolean('status')->default(false);
            $table->boolean('admin_welcome_mail_sent')->default(false);
		    $table->boolean('client_welcome_mail_sent')->default(false);
		    $table->boolean('admin_payment_mail_sent')->default(false);
		    $table->boolean('client_payment_mail_sent')->default(false);
            $table->timestamps();
        });

        Schema::table('expert_advice', function($table) {
            $table->foreign('issue_related_to_id')->references('id')->on('issue_related_tos')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expert_advice');
    }
}
