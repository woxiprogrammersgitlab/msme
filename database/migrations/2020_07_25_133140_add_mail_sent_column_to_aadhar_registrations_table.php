<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMailSentColumnToAadharRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aadhar_registrations', function (Blueprint $table) {
            $table->boolean('admin_welcome_mail_sent')->default(false)->after('form_type');
		    $table->boolean('client_welcome_mail_sent')->default(false)->after('admin_welcome_mail_sent');
		    $table->boolean('admin_payment_mail_sent')->default(false)->after('client_welcome_mail_sent');
		    $table->boolean('client_payment_mail_sent')->default(false)->after('admin_payment_mail_sent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aadhar_registrations', function (Blueprint $table) {
            //
        });
    }
}
